This project has nothing to do with the company FileCloud itself. We run FileCloud since a while in our datacenters for different customers in a traditional way with VM. We have single VM installations as well as HA installlations. Single VM installations are deployed in less than 4h by hand, HA installations take noramle 3-5 days. To automate our deploy process for this application we decided to try a deployment with docker/kubernetes. We know, it is not the easiest way, but we belive, that it is besides an automation tool like Puppet or Ansilbe the only way to be agile. 

Why to build a dockerized filecloud environment:
- Deployment time can be reduced to min instead of h
- Microservices can be changed without touching all the componentes
- Scaling up an down is fast and can be done without touching many devices like routers, firewall, load balancers etc.
- Environments can be seperated easier and faster

A good start to understand the requirements for a dockerized filecloud environment is the Filecloud HA settings:
https://www.getfilecloud.com/supportdocs/display/cloud/FileCloud+High+Availability

If everything can be dockerized is a architecture question. It would be great, but if there are doubts I'm open to discuss. 

Out of a first impression, the architecture could be done out of:
- 1 to n Application Server (Apache 2, PHP, FileConvertor and Libreoffice)
- 1 to n MongoDB Instances
- Solr Cluster
- CalmAV Antivirus (https://www.getfilecloud.com/supportdocs/display/cloud/Enable+Antivirus+Scanning)
- NGIX Reverse Proxy (https://www.getfilecloud.com/supportdocs/display/cloud/Deployment)

Connection to the "outside" world:
- LDAP connection to OpenLDAP or Active Directory
- Passwort Reset Tool like 

Original installation: https://www.getfilecloud.com/supportdocs/display/cloud/Ubuntu+Package+Installation

There are two files in the original folder:
installFileCloud.sh --> Adjusted installation by us for Filecloud for Ubuntu on Version 16.04
originalFileCloudInstall.sh --> The original script provided by filecloud

I started with a docker file, but I don't have the time to really take care about it. In the moment is everything in one file and that should not be like that. I did it to be fast and have a result early. But in this project I would really like split.
1. run it on docker
2. run it on kubernetes

To do a silent installation i also adjusted the debian packages provided by FileCloud. Because there are some unnessesary manual stops in it to ask for:
- the data directory which i changed to /data
- and the passwort for the admin account is: 7kPMURCu%JbWGRvYfRoFEQqc

The sources are in the original folder and the debian packages in the filecloud directory.

If there are any further questions don't hestitate to contact me: markus.baettig@caprilytics.com


This project automates the delivery of a filecloud environment

git clone git@gitlab.com:caprilyticsag/datahub.git
cd datahub
