#!/bin/bash
whiptail --title "FileCloud Linux Installer and Upgrader" --yesno "We are about to start installing or upgrading FileCloud on Linux" --yes-button "Proceed" --no-button "Cancel"  10 60
exitstatus=$?
if [ $exitstatus = 0 ]; then
whiptail --title "Welcome to FileCloud Linux Installer and Upgrader" --msgbox "Please click OK to indicate that you have read and agree to our Terms of Service (https://www.getfilecloud.com/tos/) and our Privacy policy (https://www.getfilecloud.com/privacy/)." 10 100
set -efu

RPM_INSTALLER_URL="http://patch.codelathe.com/tonidocloud/live/installer/file_cloud_rpm.tgz"
DEB_INSTALLER_URL="http://patch.codelathe.com/tonidocloud/live/installer/file_cloud_deb.tgz"
die()
{
        echo "ERROR: $*" >&2
        exit 1
}

verbose()
{
        if [ -n "$verbose" ]; then
                echo "$@" >&2
        fi
}

check_root()
{
        if [ `id -u` -ne 0 ]; then
                die "You should have superuser privileges to install FileCloud"
        fi
}
write_log()
{
  while read text
  do
      LOGTIME=`date "+%Y-%m-%d %H:%M:%S"`
LOG=/var/log/$(basename -- "$0" .sh ).log
if [ ! -e "$LOG" ] ; then
         touch $LOG
         chmod 744 $LOG
fi
     echo $LOGTIME": $text" | tee -a $LOG;

  done
}


dependency_check_rpm()
{
if ! [ -x "$(command -v unzip)" ]; then
echo "Unzip not installed" 
yum install unzip -y
echo "Installing Unzip" 
else
echo "Unzip Installed"
fi
if ! [ -x "$(command -v rsync)" ]; then
echo "Rsync not installed" 
yum install rsync -y
echo "Installing Rsync" 
else
echo "Rsync Installed"
fi
if ! [ -x "$(command -v java)" ]; then
echo "Java not installed" 
yum install java-1.8.0-openjdk -y
echo "Installing Java" 
else
echo "Java Installed"
fi
}

dependency_check_deb()
{
if ! [ -x "$(command -v unzip)" ]; then
echo "Unzip not installed"
apt-get install unzip -y
echo "Installing Unzip"
else
echo "Unzip Installed"
fi
if ! [ -x "$(command -v java)" ]; then
echo "Java not installed"
sudo apt-get install software-properties-common  -y
sudo add-apt-repository "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main"
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
sudo apt-get update
sudo apt-get install oracle-java8-installer -y
echo "Installing Java"
else
version=$(java -version 2>&1 | awk -F '"' '/version/ {print $2}')
echo version "$version"
if [[ "$version" > "1.7" ]]; then
    echo  "Java version OK"  >> /var/log/solr_installer.log 2>&1
else
    echo  "Java version is less than 1.7. Please upgrade you JRE version."  >> /var/log/solr_installer.log  2>&1
    exit 0
fi
fi
}
ubuntu_package_installer()
{

### Add all repositories ############################


echo -e "\n### Adding PHP repository ###############\n" 2>&1 | write_log > /dev/null
sudo apt-get update -y
sudo apt-get install software-properties-common -y
sudo add-apt-repository ppa:ondrej/php -y 2>&1 | write_log > /dev/null

### Install Webserver ###################################

echo -e "\n### Installing apache #############\n" 2>&1 | write_log > /dev/null
sudo apt-get update -y 2>&1 | write_log > /dev/null
sudo apt-get install unzip -y  2>&1 | write_log > /dev/null
sudo apt-get install apache2 build-essential libsslcommon2-dev libssl-dev pkg-config memcached -y 2>&1 | write_log 


### Install PHP 7.1 #################################

echo -e "\n### Installing PHP 7.1 #############\n" 2>&1 | write_log > /dev/null
sudo apt-get install php7.1 php7.1-cli php7.1-common -y 2>&1 | write_log 
sudo apt-get install php-pear -y  2>&1 | write_log 
sudo apt-get install php-dev -y 2>&1 | write_log 

### Install PHP extensions required by Laravel

echo -e "\n### Installing PHP extensions ######\n" 2>&1 | write_log > /dev/null
sudo apt-get install php7.1-json php7.1-opcache php7.1-mbstring php7.1-mcrypt php7.1-zip php7.1-memcache php7.1-xml php7.1-bcmath libapache2-mod-php7.1 php7.1-gd php7.1-curl php7.1-ldap php7.1-gmp php7.1-intl unzip rsync curl -y 2>&1 | write_log

### Install MongoDB driver for PHP 7.1 ##############

echo -e "\n### Installing MongoDB PHP driver ##\n" 2>&1 | write_log > /dev/null
sudo apt-get install php-mongodb -y 2>&1 | write_log 


set +e

echo "* Disabling Apache PHP 5 module..." | write_log
sudo a2dismod php5 > /dev/null  2>&1

echo "* Disabling Apache PHP 5.6 module..." | write_log
sudo a2dismod php5.6 > /dev/null  2>&1

echo "* Disabling Apache PHP 7.1 module..." | write_log
sudo a2dismod php7.1 > /dev/null  2>&1

echo "* Disabling Apache PHP 7.0 module..." | write_log
sudo a2dismod php7.0 > /dev/null  2>&1

echo "* Enabling Apache PHP 7.1 module..." 2>&1 | write_log
sudo a2enmod php7.1 2>&1 | write_log > /dev/null

echo "* Restarting Apache..." 2>&1 | write_log
sudo service apache2 restart 2>&1 | write_log > /dev/null

echo "* Switching CLI PHP to 7.1..." 2>&1 | write_log
sudo update-alternatives --set php /usr/bin/php7.1 2>&1 | write_log > /dev/null

echo "* Switch to PHP 7.1 complete." 2>&1 | write_log
set -e

echo -e "\n### Installing IONCUBE PHP driver ##\n" 2>&1 | write_log > /dev/null
mkdir -p /tmp/ioncubeinstall && cd "$_" 
wget http://patch.codelathe.com/tonidocloud/live/ioncube/ioncube_loaders_lin_x86-64.zip
echo "* Ioncube Downloaded Successfully..." 2>&1 | write_log
unzip ioncube_loaders_lin_x86-64.zip 2>&1 | write_log
cd ioncube/
echo -e "\n### COPYING IONCUBE ##\n" 2>&1 | write_log > /dev/null
cp -rvf /tmp/ioncubeinstall/ioncube/ /usr/lib/php/7.1/  2>&1 | write_log > /dev/null
echo "zend_extension = '/usr/lib/php/7.1/ioncube/ioncube_loader_lin_7.1.so'" > /etc/php/7.1/mods-available/01-ioncube.ini 2>&1 | write_log > /dev/null
ln -sfn /etc/php/7.1/mods-available/01-ioncube.ini /etc/php/7.1/apache2/conf.d/ 2>&1 | write_log > /dev/null
ln -sfn /etc/php/7.1/mods-available/01-ioncube.ini /etc/php/7.1/cli/conf.d/ 2>&1 | write_log > /dev/null
service apache2 restart  2>&1 | write_log > /dev/null
echo -e "\n### IONCUBE PHP driver Installed Successfully##\n" 2>&1 | write_log > /dev/null

### Verify installs #################################

echo -e "" 2>&1 | write_log > /dev/null
echo -e "###############################" 2>&1 | write_log > /dev/null
echo -e "### VERIFY THE OUTPUT BELOW ###" 2>&1 | write_log > /dev/null
echo -e "###############################" 2>&1 | write_log > /dev/null
echo -e "\n### PHP ###############\n" 2>&1 | write_log > /dev/null
echo -e "$(php -v)" 2>&1 | write_log > /dev/null

rm -rvf /tmp/ioncubeinstall 2>&1 | write_log > /dev/null

# Import the Public Key used by the Ubuntu Package Manager
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6 2>&1 | write_log > /dev/null

# Create a file list for mongoDB to fetch the current repository
echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.4.list 2>&1 | write_log > /dev/null

# Update the Ubuntu Packages
apt update 2>&1 | write_log

# Install MongoDB
		apt install mongodb-org -y 2>&1 | write_log
		systemctl enable mongod 2>&1 | write_log
        systemctl start mongod 2>&1 | write_log
        systemctl restart apache2 2>&1 | write_log
}
jessie_package_installer()
{
apt-get install sudo rsync curl -y 2>&1 | write_log
sudo apt-get install apt-transport-https lsb-release ca-certificates -y 2>&1 | write_log
sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg 2>&1 | write_log
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list 2>&1 | write_log

sudo apt-get update 2>&1 | write_log
sudo apt-get install unzip -y 2>&1 | write_log
sudo apt-get install apache2 build-essential libssl-dev pkg-config memcached -y 2>&1 | write_log
echo -e "\n### Installing PHP 7.1 #############\n"; 2>&1 | write_log
sudo apt-get install php7.1 php7.1-cli php7.1-common -y 2>&1 | write_log
sudo apt-get install php-pear -y 2>&1 | write_log
sudo apt-get install php-dev -y 2>&1 | write_log

### Install PHP extensions required by Laravel

echo -e "\n### Installing PHP extensions ######\n";
sudo apt-get install php7.1-json php7.1-opcache php7.1-mbstring php7.1-memcache php7.1-mcrypt php7.1-zip php7.1-xml php7.1-bcmath libapache2-mod-php7.1 php7.1-gd php7.1-curl php7.1-ldap php7.1-gmp php7.1-intl php-memcache  -y 2>&1 | write_log

### Install MongoDB driver for PHP 7.1 ##############
 
echo -e "\n### Installing MongoDB PHP driver ##\n"; 2>&1 | write_log
sudo apt-get install php-mongodb -y 2>&1 | write_log

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6 2>&1 | write_log
echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.4 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list 2>&1 | write_log
sudo apt-get update 2>&1 | write_log
sudo apt-get install -y mongodb-org 2>&1 | write_log
sudo service mongod start 2>&1 | write_log
set +e
echo "* Disabling Apache PHP 5 module..." | write_log
sudo a2dismod php5 > /dev/null  2>&1

echo "* Disabling Apache PHP 5.6 module..." | write_log
sudo a2dismod php5.6 > /dev/null  2>&1

echo "* Disabling Apache PHP 7.1 module..." | write_log
sudo a2dismod php7.1 > /dev/null  2>&1

echo "* Disabling Apache PHP 7.0 module..." | write_log
sudo a2dismod php7.0 > /dev/null  2>&1

echo "* Enabling Apache PHP 7.1 module..." 2>&1 | write_log
sudo a2enmod php7.1 2>&1 | write_log > /dev/null

echo "* Restarting Apache..." 2>&1 | write_log
sudo service apache2 restart 2>&1 | write_log > /dev/null

echo "* Switching CLI PHP to 7.1..." 2>&1 | write_log
sudo update-alternatives --set php /usr/bin/php7.1 2>&1 | write_log > /dev/null

echo "* Switch to PHP 7.1 complete." 2>&1 | write_log
set -e
echo -e "\n### Installing IONCUBE PHP driver ##\n" 2>&1 | write_log > /dev/null
mkdir -p /tmp/ioncubeinstall && cd "$_" 
wget http://patch.codelathe.com/tonidocloud/live/ioncube/ioncube_loaders_lin_x86-64.zip
echo "* Ioncube Downloaded Successfully..." 2>&1 | write_log
unzip ioncube_loaders_lin_x86-64.zip 2>&1 | write_log
cd ioncube/
echo -e "\n### COPYING IONCUBE ##\n" 2>&1 | write_log > /dev/null
cp -rvf /tmp/ioncubeinstall/ioncube/ /usr/lib/php/7.1/  2>&1 | write_log > /dev/null
echo "zend_extension = '/usr/lib/php/7.1/ioncube/ioncube_loader_lin_7.1.so'" > /etc/php/7.1/mods-available/01-ioncube.ini 2>&1 | write_log > /dev/null
ln -sfn /etc/php/7.1/mods-available/01-ioncube.ini /etc/php/7.1/apache2/conf.d/ 2>&1 | write_log > /dev/null
ln -sfn /etc/php/7.1/mods-available/01-ioncube.ini /etc/php/7.1/cli/conf.d/ 2>&1 | write_log > /dev/null
service apache2 restart  2>&1 | write_log > /dev/null
echo -e "\n### IONCUBE PHP driver Installed Successfully##\n" 2>&1 | write_log > /dev/null
}
rpm_package_installer()
{
if grep -q -i "release 6" /etc/redhat-release
then
  echo "Server running in CentOS/RHEL 6.x"
   cat > /etc/yum.repos.d/remi.repo << \EOF
# This repository is safe to use with RHEL/CentOS base repository
# it only provides additional packages for the PHP stack
# all dependencies are in base repository or in EPEL

[remi-safe]
name=Safe Remi's RPM repository for Enterprise Linux $releasever - $basearch
#baseurl=http://rpms.remirepo.net/enterprise/$releasever/safe/$basearch/
mirrorlist=http://cdn.remirepo.net/enterprise/$releasever/safe/mirror
enabled=1
gpgcheck=1
gpgkey=http://rpms.remirepo.net/RPM-GPG-KEY-remi


EOF
  rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm 2>&1 | write_log
  rpm --import https://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-6 2>&1 | write_log
  fi

  if grep -q -i "release 7" /etc/redhat-release
then
  echo "Server running in CentOS/RHEL 7.x"
  cat > /etc/yum.repos.d/remi.repo << \EOF
# This repository is safe to use with RHEL/CentOS base repository
# it only provides additional packages for the PHP stack
# all dependencies are in base repository or in EPEL

[remi-safe]
name=Safe Remi's RPM repository for Enterprise Linux $releasever - $basearch
#baseurl=http://rpms.remirepo.net/enterprise/$releasever/safe/$basearch/
mirrorlist=http://cdn.remirepo.net/enterprise/$releasever/safe/mirror
enabled=1
gpgcheck=1
gpgkey=http://rpms.remirepo.net/RPM-GPG-KEY-remi


EOF
  rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm 2>&1 | write_log 
  rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm 2>&1 | write_log
  rpm --import http://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-7 2>&1 | write_log
  fi
yum update -y  2>&1 | write_log
cat <<EOF > /etc/yum.repos.d/mongodb-org-3.4.repo
[mongodb-org-3.4]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/\$releasever/mongodb-org/3.4/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-3.4.asc
EOF
        #if we are here, php repo is added successfully
		yum install yum-utils -y


        echo "Installing pre-reqs"  2>&1 | write_log
yum install -y httpd mod_ssl php71w php71w-gd php71w-pear php71w-devel php71w-bcmath php71w-mbstring php71w-xml php71w-curl php71w-intl php71w-ldap php71w-pecl-memcached php71w-gmp gcc make patch memcached openssl-devel wget unzip  2>&1 | write_log
yum --enablerepo=remi-safe,remi-php71 install php71-php-pecl-memcache -y 2>&1 | write_log
        echo "Adding MongoDB 3.4" 2>&1 | write_log
        yum install -y mongodb-org php71w-pecl-mongodb  2>&1 | write_log
		#Binding Memcache to 127.0.0.1 and disabling UDP
		echo "Securing Memcache" 2>&1 | write_log
		sed -i -e "s/^OPTIONS/#OPTIONS/" /etc/sysconfig/memcached 2>&1 | write_log
		echo 'OPTIONS="-l 127.0.0.1 -U 0"' >> /etc/sysconfig/memcached 2>&1 | write_log
		systemctl enable memcached 2>&1 | write_log
		systemctl start memcached 2>&1 | write_log
		
}
rpm_ioncube_installer()
{
mkdir -p /tmp/ioncubeinstall && cd "$_" 
wget http://patch.codelathe.com/tonidocloud/live/ioncube/ioncube_loaders_lin_x86-64.zip  2>&1 | write_log
unzip ioncube_loaders_lin_x86-64.zip 2>&1 | write_log
rm -rvf /usr/lib64/php/modules/ioncube_loader_lin_* /etc/php.d/20ioncube.ini 2>&1 | write_log
cp -rvf /tmp/ioncubeinstall/ioncube/ /usr/lib64/php/modules 2>&1 | write_log
echo "zend_extension = '/usr/lib64/php/modules/ioncube/ioncube_loader_lin_7.1.so'" > /etc/php.d/20ioncube.ini 2>&1 | write_log
cp /opt/remi/php71/root/usr/lib64/php/modules/memcache.so /usr/lib64/php/modules/
echo "extension=memcache.so " > /etc/php.d/memcache.ini
service httpd restart
}
centos_installer()
{

whiptail --title "FileCloud Linux Installer" --yesno "Filecloud installation will proceed with installing Apache , PHP and MongoDB." --yes-button "Proceed" --no-button "Cancel"  10 60

exitstatus=$?
if [ $exitstatus = 0 ]; then

rpm_package_installer

cd /usr/local/src
			{
    wget -N "$RPM_INSTALLER_URL" 2>&1 | \
 stdbuf -o0 awk '/[.] +[0-9][0-9]?[0-9]?%/ { print substr($0,63,3) }'
} | whiptail --gauge "Downloading FileCloud" 7 60 0
		tar -xzvf file_cloud_rpm.tgz
		
  echo "Installing FileCloud package"  2>&1 | write_log
        yum -y --nogpgcheck localinstall filecloud-1.0.0-1.noarch.rpm 2>&1 | write_log

        echo "Configuring filecloud..."  2>&1 | write_log
        bash /tmp/filecloud/config.sh  2>&1 | write_log
rpm_ioncube_installer
else

 whiptail --title "Contact Us" --msgbox "If you need any help, please contact us at support@codelathe.com" 10 60
 exit 0;

fi
}

ubuntu_installer()
{
whiptail --title "FileCloud Linux Installer" --yesno "Filecloud installation will proceed with installing Apache , PHP and MongoDB." --yes-button "Proceed" --no-button "Cancel"  10 60
exitstatus=$?
if [ $exitstatus = 0 ]; then


set -e

distro()
{
        DISTRO=`lsb_release -si`
        RELEASE=`lsb_release -sc`
}

handleUbuntuPrecise()
{
        echo "Ubuntu 12.04.x not supported. Please try installation on Ubuntu 16.04 LTS";
        exit 1;
}
handleUbuntuTrusty()
{
		echo "Ubuntu 14.04.x not supported. Please try installation on Ubuntu 16.04 LTS";
        exit 1;
}

handleUbuntuXenial()
{
ubuntu_package_installer

		cd /usr/local/src
			{
			
    wget -N "$DEB_INSTALLER_URL" 2>&1 | \
 stdbuf -o0 awk '/[.] +[0-9][0-9]?[0-9]?%/ { print substr($0,63,3) }'
} | whiptail --gauge "Downloading FileCloud" 7 60 0
		tar -xzvf file_cloud_deb.tgz 2>&1 | write_log
        dpkg -i ./filecloudprereq.deb 2>&1 | write_log
        dpkg -i ./filecloudconf.deb 2>&1 | write_log
		cd /usr/local/src 
		rm -rvf *.deb 
}

handleDebianJessie()
{
jessie_package_installer
		cd /usr/local/src
			{
			
   wget -N "$DEB_INSTALLER_URL" 2>&1 | \
 stdbuf -o0 awk '/[.] +[0-9][0-9]?[0-9]?%/ { print substr($0,63,3) }'
} | whiptail --gauge "Downloading FileCloud" 7 60 0
		tar -xzvf file_cloud_deb.tgz
        dpkg -i ./filecloudprereq.deb
        apt-get update
        service mongod restart
        service apache2 restart
        dpkg -i ./filecloudconf.deb
		cd /usr/local/src
		rm -rvf *.deb
}
handleUbuntuBionic()
{
ubuntu_package_installer

		cd /usr/local/src
			{
			
    wget -N "$DEB_INSTALLER_URL" 2>&1 | \
 stdbuf -o0 awk '/[.] +[0-9][0-9]?[0-9]?%/ { print substr($0,63,3) }'
} | whiptail --gauge "Downloading FileCloud" 7 60 0
		tar -xzvf file_cloud_deb.tgz 2>&1 | write_log
        dpkg -i ./filecloudprereq.deb 2>&1 | write_log
        dpkg -i ./filecloudconf.deb 2>&1 | write_log
		cd /usr/local/src 
		rm -rvf *.deb 
}
install()
{
        distro
        if [ "$DISTRO" = "Ubuntu" ]
        then
                if [ "$RELEASE" = "precise" ]
                then
                       handleUbuntuPrecise
                elif [ "$RELEASE" = "trusty" ]
                then
                       handleUbuntuTrusty
                elif [ "$RELEASE" = "xenial" ]
                then
                       handleUbuntuXenial
                elif [ "$RELEASE" = "bionic" ]
				then
				handleUbuntuBionic
				fi
        elif [ "$DISTRO" = "Debian" ]
        then
                if [ "$RELEASE" = "jessie" ]
                then
                       handleDebianJessie
                fi
        fi
}
install
else

 whiptail --title "Contact Us" --msgbox "If you need any help, please contact us at support@codelathe.com" 10 60
 exit 0;
fi
}
docconvertor_installer()
{
echo -e "" | write_log > /dev/null
echo -e "###############################" | write_log > /dev/null
echo -e "### INSTALLING DOCCONVERTOR ###" | write_log > /dev/null
echo -e "###############################"| write_log > /dev/null

whiptail --title "Welcome to Filecloud Docconvertor Linux Installer" --msgbox "Please Press OK to continue the Installation." 8 78

{
    for ((i = 0 ; i <= 100 ; i+=20)); do
        sleep 1
        echo $i
    done
} | whiptail --gauge "Setting up Filecloud Docconvertor Installation" 7 60 0
######UBUNTU INSTALL STARTS##########	

if [ -n "$(command -v apt-get)" ]; then
		
sudo mkdir -p /usr/local/src/libreoffice 2>&1 | write_log
sudo apt-get install libxinerama1 libcairo2 libglu1-mesa libcups2 libsm6 -y 2>&1 | write_log
cd /usr/local/src/libreoffice
                        {
    URL1="http://patch.codelathe.com/tonidocloud/live/libreoffice/LibreOffice_6.0.2_Linux_x86-64_deb.tar.gz"

sudo wget -N "$URL1" -O LibreOffice.tar.gz 2>&1 | \
 stdbuf -o0 awk '/[.] +[0-9][0-9]?[0-9]?%/ { print substr($0,63,3) }'
} | whiptail --gauge "Downloading FileCloud DOCCONVERTOR" 7 60 0
sudo tar xvf LibreOffice.tar.gz 2>&1 | write_log
cd LibreOffice_6.0.2.1_Linux_x86-64_deb/DEBS 
echo "Installing LibreOffice ..." 2>&1 | write_log
set +e
sudo dpkg -R --install /usr/local/src/libreoffice/LibreOffice_6.0.2.1_Linux_x86-64_deb/DEBS/ 2>&1 | write_log
set -e
echo "Removing LibreOffice temp files" 2>&1 | write_log
ln -s /usr/bin/libreoffice6.0 /usr/bin/libreoffice 2>&1 | write_log
ln -s /opt/libreoffice6.0 /usr/lib/libreoffice 2>&1 | write_log
whiptail --textbox /dev/stdin 10 100 <<<"$(echo "Installing DOCCONVERTOR Service")"
mkdir -p /opt/fcdocconverter 2>&1 | write_log
cd /opt/fcdocconverter && wget http://patch.codelathe.com/tonidocloud/live/3rdparty/fcdocconverter/FCDocConverter.jar 2>&1 | write_log

cd /etc/init.d && wget http://patch.codelathe.com/tonidocloud/live/3rdparty/fcdocconverter/fcdocconverter 2>&1 | write_log
sudo chmod u+x /etc/init.d/fcdocconverter 2>&1 | write_log
sudo update-rc.d fcdocconverter defaults 2>&1 | write_log
sudo service fcdocconverter start 2>&1 | write_log
fi
######UBUNTU INSTALL ENDS##########	


######CENTOS/RHEL INSTALL STARTS##########	
	
	if [ -n "$(command -v yum)" ]; then
		 set -euo pipefail

# Grab LibreOffice 6.0.1 -- version in centos repo does not work.
mkdir -p /tmp/libreoffice 2>&1 | write_log
yum install libXinerama cairo cups-libs -y 2>&1 | write_log
cd /tmp/libreoffice 

                        {
    URL2="http://patch.codelathe.com/tonidocloud/live/libreoffice/LibreOffice_6.0.2_Linux_x86-64_rpm.tar.gz"

wget -N "$URL2" -O LibreOffice.tar.gz 2>&1 | \
 stdbuf -o0 awk '/[.] +[0-9][0-9]?[0-9]?%/ { print substr($0,63,3) }'
} | whiptail --gauge "Downloading FileCloud DOCCONVERTOR" 7 60 0
tar xvf LibreOffice.tar.gz 2>&1 | write_log
cd /tmp/libreoffice/LibreOffice_6.0.2.1_Linux_x86-64_rpm/RPMS 2>&1 | write_log
echo "Installing LibreOffice ..." 2>&1 | write_log
set +e
rpm -ivh /tmp/libreoffice/LibreOffice_6.0.2.1_Linux_x86-64_rpm/RPMS/*.rpm 2>&1 | write_log
set -e 
ln -s /usr/bin/libreoffice6.0 /usr/bin/libreoffice 2>&1 | write_log
ln -s /opt/libreoffice6.0 /usr/lib/libreoffice 2>&1 | write_log
echo "Removing LibreOffice temp files" 2>&1 | write_log
#rm -rf /usr/local/src/libreoffice 2>&1 | write_log
echo "Downloading FileCloud Document Converter wrapper.." 2>&1 | write_log
        mkdir -p /opt/fcdocconverter/ 
        curl 'http://patch.codelathe.com/tonidocloud/live/3rdparty/fcdocconverter/FCDocConverter.jar' -o /opt/fcdocconverter/FCDocConverter.jar 2>&1 | write_log

		 echo "Creating systemd service unit for FCDocConverter.." 2>&1 | write_log
        cat > /etc/systemd/system/fcdocconverter.service <<EOL 
        [Unit]
        Description=FileCloud Doc Converter service
        After=network.target httpd.service

        [Service]
        Type=simple
        ExecStart=/bin/sh -c '/usr/bin/env java -Djava.library.path="/usr/lib/libreoffice/program/" -jar /opt/fcdocconverter/FCDocConverter.jar'
        WorkingDirectory=/tmp/
        PrivateTmp=yes
        User=root
        Restart=always
        SuccessExitStatus=143

        [Install]
        WantedBy=multi-user.target
EOL

        echo "Enabling and starting fcdocconverter." 2>&1 | write_log
        systemctl daemon-reload 2>&1 | write_log
        systemctl enable fcdocconverter 2>&1 | write_log
        systemctl restart fcdocconverter 2>&1 | write_log
fi
######CENTOS/RHEL INSTALL ENDS##########	
}


solr_installer()
{
echo -e "" | write_log > /dev/null
echo -e "###############################" | write_log > /dev/null
echo -e "### INSTALLING SOLR         ###" | write_log > /dev/null
echo -e "###############################"| write_log > /dev/null
sudo mkdir -p /usr/local/src/solrinstall 2>&1 | write_log

SOLR_URL="http://archive.apache.org/dist/lucene/solr/5.3.1/solr-5.3.1.tgz"
SOLR_DOWNLOAD_DIR=/usr/local/src/solrinstall
whiptail --title "Welcome to Filecloud Content Search Linux Installer" --msgbox "Please Press OK to continue the Installation." 8 78
if [ -n "$(command -v yum)" ]; then
 dependency_check_rpm
fi
if [ -n "$(command -v apt-get)" ]; then
 dependency_check_deb
fi
echo  "Java Installation Successful"  | write_log > /dev/null

echo  "Downloading Solr zip" | write_log > /dev/null
cd ${SOLR_DOWNLOAD_DIR}
  {  
sudo wget -N "$SOLR_URL" 2>&1 | \
 stdbuf -o0 awk '/[.] +[0-9][0-9]?[0-9]?%/ { print substr($0,63,3) }'
} | whiptail --gauge "Downloading Solr " 7 60 0

echo  "Downloaded Solr zip"  | write_log > /dev/null
sudo tar --wildcards -xzvf solr-5.3.1.tgz solr-5.3.1/bin/install_solr_service.sh 2>&1 | write_log
sudo cp solr-5.3.1/bin/install_solr_service.sh ./ 2>&1 | write_log
sudo bash ./install_solr_service.sh solr-5.3.1.tgz 2>&1 | write_log

if [ -f /etc/lsb-release ]
then
        sudo update-rc.d solr defaults 2>&1 | write_log
else
        chkconfig solr on 2>&1 | write_log
fi
sudo service solr restart 2>&1 | write_log
echo  "Installation Complete" | write_log > /dev/null
sudo mkdir -p /var/solr/data/fccore | write_log > /dev/null
sudo rsync -r /var/www/html/thirdparty/solarium/fcskel/ /var/solr/data/fccore/  | write_log > /dev/null
sudo chown solr.solr /var/solr/data/fccore -Rf | write_log > /dev/null
}


new_install()
{
 if [ -f /etc/lsb-release ]; then
                os_name=$(lsb_release -s -d)
        elif [ -f /etc/debian_version ]; then
                os_name="Debian $(cat /etc/debian_version)"
        elif [ -f /etc/redhat-release ]; then
                os_name=`cat /etc/redhat-release`
        else
                os_name="$(cat /etc/*release | grep '^PRETTY_NAME=\|^NAME=\|^DISTRIB_ID=' | awk -F\= '{print $2}' | tr -d '"' | tac)"
                if [ -z "$os_name" ]; then
                        os_name="$(uname -s)"
                fi
        fi

		

 {
    for ((i = 0 ; i <= 100 ; i+=20)); do
        sleep 1
        echo $i
    done
} | whiptail --gauge "$os_name detected. Setting up FileCloud installation" 7 60 0

if [ -n "$(command -v yum)" ]; then

 centos_installer
fi

if [ -n "$(command -v apt-get)" ]; then
 ubuntu_installer
 a2enmod headers
 service apache2 restart
 sudo chmod +x /var/www/html/thirdparty/prop/p23l

fi
}


binary_file()
{
BINARY_URL="http://patch.codelathe.com/tonidocloud/live/installer/filecloudcp"
sudo curl -o /usr/bin/filecloudcp $BINARY_URL > /dev/null
sudo chmod 744 /usr/bin/filecloudcp

}
checkupgradefile()
{
cat > /usr/local/src/checkupgrade.php << \EOF
<?php
define('TONIDO_CLOUD_ROOT_DIR', realpath('/var/www/html'));
 require_once(TONIDO_CLOUD_ROOT_DIR . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'framework' . DIRECTORY_SEPARATOR . 'runtime.php');


   $local = "0.0.0.0";
            $remote = "0.0.0.0";
                $fcheck = 1;
            
            $infoArray = array();
            $retcode = \core\framework\UpdateCheckManager::isBuildUptodate($local,$remote,$infoArray, $fcheck);
             if ($retcode === 1) // up to date
                {
                   echo "You are running the latest version of Filecloud v$local";
                }
                else if ($retcode === 2) // update available
                {
					
                    echo "New Filecloud Upgrade is available to v$remote";
                    $updatestring = '2';
					file_put_contents('/usr/local/src/upgrade_available.txt', $updatestring);					
                }
                else if ($retcode === 0) // error
                {
                    echo "ERROR";
                }


EOF
}
removeupgradefile()
{
rm -rf /usr/local/src/checkupgrade.php
if [ -e /usr/local/src/upgrade_available.txt ]; then
rm -rf /usr/local/src/upgrade_available.txt
fi
}
upgrade_available()
{
checkupgradefile
whiptail --title "FileCloud Control Panel" --textbox /dev/stdin 10 100 <<<"$(/usr/bin/php -f /usr/local/src/checkupgrade.php | sed -e 's/^[ \t]*//' | tr -d '\r')"
if [ -e /usr/local/src/upgrade_available.txt ]; then
CHECKUPDATE=$(cat /usr/local/src/upgrade_available.txt | sed -e 's/^[ \t]*//' |  tr -d '\r')
if [ $CHECKUPDATE == 2 ]; then
UPGRADE_SCRIPT_URL=http://patch.codelathe.com/tonidocloud/live/installer/upgrader.sh
curl -s $UPGRADE_SCRIPT_URL | bash -s --
fi
fi
removeupgradefile
}


#Looking for older installations
if [[ -e /var/www/html/config/cloudconfig.php && /var/www/config/cloudconfig.php ]]; then
            whiptail --textbox /dev/stdin 10 100 <<<"$(echo "FileCloud installation exists")"
            upgrade_available
else
new_install
solr_installer
docconvertor_installer
binary_file
echo "Adding CronJob" | write_log
if [ -n "$(command -v yum)" ]; then

 echo "*/5 * * * * php /var/www/html/core/framework/cron.php"  | crontab -u apache - 2>/dev/null
 crontab -u apache -l 
fi

if [ -n "$(command -v apt-get)" ]; then
 
echo "*/5 * * * * php /var/www/html/core/framework/cron.php" | crontab -u www-data -  2>/dev/null
crontab -u www-data -l 
fi
chmod +x /var/www/html/thirdparty/prop/*
whiptail --textbox /dev/stdin 10 100 <<<"$(echo "FileCloud Installation completed")"
exit 0
fi
else

 whiptail --title "FileCloud Linux Installer" --msgbox "Installation/upgrade has been canceled. If this operation was canceled abruptly due to an error, please contact us at support@codelathe.com." 10 60
 exit 0;
fi

