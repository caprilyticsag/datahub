#!/bin/bash

DEB_INSTALLER_URL="http://patch.codelathe.com/tonidocloud/live/installer/file_cloud_deb.tgz"


write_log(){
	while read text
	do
		LOGTIME=`date "+%Y-%m-%d %H:%M:%S"`
		LOG=./$(basename -- "$0" .sh ).log
		if [ ! -e "$LOG" ] ; then
	    	sudo touch $LOG
	        sudo chmod 744 $LOG
		fi
	    	sudo echo $LOGTIME": $text" | sudo tee -a $LOG;
	  done
}


ubuntu_package_dependency_installer(){
	### Get Distro Version ############################
	sudo lsb_release -a 2>&1 | write_log > /dev/null

	### Update Distro ########################
	sudo apt-get update
	sudo apt-get upgrade -y

	### Add Java ############################
	sudo apt-get install software-properties-common  -y
	sudo add-apt-repository "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main"
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
	sudo apt-get update
	sudo echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | sudo debconf-set-selections
	sudo apt-get install oracle-java8-installer -y

	### Add all repositories ############################
	sudo apt-get update -y 2>&1 | write_log > /dev/null
	sudo apt-get install software-properties-common -y
	sudo add-apt-repository ppa:ondrej/php -y 2>&1 | write_log > /dev/null

	### Install Webserver ###################################
	sudo apt-get update -y 2>&1 | write_log > /dev/null
	sudo apt-get install unzip -y  2>&1 | write_log > /dev/null
	sudo apt-get install apache2 build-essential libsslcommon2-dev libssl-dev pkg-config memcached -y 2>&1 | write_log 

	### Install PHP 7.1 #################################
	sudo apt-get install php7.1 php7.1-cli php7.1-common -y 2>&1 | write_log 
	sudo apt-get install php-pear -y  2>&1 | write_log 
	sudo apt-get install php-dev -y 2>&1 | write_log 

	### Install PHP extensions required by Laravel
	sudo apt-get install php7.1-json php7.1-opcache php7.1-mbstring php7.1-mcrypt php7.1-zip php7.1-memcache php7.1-xml php7.1-bcmath libapache2-mod-php7.1 php7.1-gd php7.1-curl php7.1-ldap php7.1-gmp php7.1-intl unzip rsync curl -y 2>&1 | write_log

	### Install MongoDB driver for PHP 7.1 ##############
	sudo apt-get install php-mongodb -y 2>&1 | write_log 

	### Clean up Installation ###########################
	sudo service apache2 restart 2>&1 | write_log > /dev/null

	## --- Switching CLI PHP to 7.1...
	sudo update-alternatives --set php /usr/bin/php7.1 2>&1 | write_log > /dev/null

	## --- Installing IONCUBE PHP driver 
	sudo mkdir -p /tmp/ioncubeinstall && cd "$_" 
	sudo wget http://patch.codelathe.com/tonidocloud/live/ioncube/ioncube_loaders_lin_x86-64.zip
	sudo unzip ioncube_loaders_lin_x86-64.zip 2>&1 | write_log
	cd /tmp/ioncubeinstall/ioncube/
	sudo cp -rvf /tmp/ioncubeinstall/ioncube/ /usr/lib/php/7.1/  2>&1 | write_log > /dev/null
	sudo touch /etc/php/7.1/mods-available/01-ioncube.ini
	sudo chmod 777 /etc/php/7.1/mods-available/01-ioncube.ini
	sudo echo "zend_extension = '/usr/lib/php/7.1/ioncube/ioncube_loader_lin_7.1.so'" > /etc/php/7.1/mods-available/01-ioncube.ini 2>&1 | write_log > /dev/null
	sudo chmod 644 /etc/php/7.1/mods-available/01-ioncube.ini
	sudo ln -sfn /etc/php/7.1/mods-available/01-ioncube.ini /etc/php/7.1/apache2/conf.d/ 2>&1 | write_log > /dev/null
	sudo ln -sfn /etc/php/7.1/mods-available/01-ioncube.ini /etc/php/7.1/cli/conf.d/ 2>&1 | write_log > /dev/null
	sudo service apache2 restart  2>&1 | write_log > /dev/null

	### Verify installs #################################
	sudo echo -e "$(php -v)" 2>&1 | write_log > /dev/null
	sudo rm -rvf /tmp/ioncubeinstall 2>&1 | write_log > /dev/null

	# Import the Public Key used by the Ubuntu Package Manager
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6 2>&1 | write_log > /dev/null

	# Create a file list for mongoDB to fetch the current repository
	sudo touch /etc/apt/sources.list.d/mongodb-org-3.4.list
	sudo chmod 777 /etc/apt/sources.list.d/mongodb-org-3.4.list
	sudo echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" >> /etc/apt/sources.list.d/mongodb-org-3.4.list 2>&1 | write_log > /dev/null
	sudo chmod 644 /etc/apt/sources.list.d/mongodb-org-3.4.list

	# Update the Ubuntu Packages
	sudo apt-get update 2>&1 | write_log

	# Install MongoDB
	sudo apt-get install mongodb-org -y 2>&1 | write_log
	sudo systemctl enable mongod 2>&1 | write_log
	sudo systemctl start mongod 2>&1 | write_log
	sudo systemctl restart apache2 2>&1 | write_log
}

installFileCloud(){
	cd /usr/local/src 2>&1 | write_log
	sudo wget -N "$DEB_INSTALLER_URL" 2>&1 | write_log
	sudo tar -xzvf file_cloud_deb.tgz 2>&1 | write_log
	sudo dpkg -i ./filecloudprereq.deb 2>&1 | write_log
	sudo dpkg -i ./filecloudconf.deb 2>&1 | write_lo
	cd /usr/local/src 2>&1 | write_log
	sudo rm -rvf *.deb 2>&1 | write_lo
}

docconvertor_installer(){
	### Install LibreOffice
	sudo mkdir -p /usr/local/src/libreoffice 2>&1 | write_log
	sudo apt-get install libxinerama1 libcairo2 libglu1-mesa libcups2 libsm6 -y 2>&1 | write_log
	cd /usr/local/src/libreoffice

	URL1="http://patch.codelathe.com/tonidocloud/live/libreoffice/LibreOffice_6.0.2_Linux_x86-64_deb.tar.gz"
	sudo wget -N "$URL1" -O LibreOffice.tar.gz 2>&1 | write_log
	sudo tar -xvf LibreOffice.tar.gz 2>&1 | write_log
	sudo dpkg -R --install /usr/local/src/libreoffice/LibreOffice_6.0.2.1_Linux_x86-64_deb/DEBS/ 2>&1 | write_log
	sudo ln -s /usr/bin/libreoffice6.0 /usr/bin/libreoffice 2>&1 | write_log
	sudo ln -s /opt/libreoffice6.0 /usr/lib/libreoffice 2>&1 | write_lo

	sudo mkdir -p /opt/fcdocconverter 2>&1 | write_log
	cd /opt/fcdocconverter 2>&1 | write_log
	sudo wget http://patch.codelathe.com/tonidocloud/live/3rdparty/fcdocconverter/FCDocConverter.jar 2>&1 | write_log
	cd /etc/init.d 2>&1 | write_log
	sudo wget http://patch.codelathe.com/tonidocloud/live/3rdparty/fcdocconverter/fcdocconverter 2>&1 | write_log
	sudo chmod u+x /etc/init.d/fcdocconverter 2>&1 | write_log
	sudo update-rc.d fcdocconverter defaults 2>&1 | write_log
	sudo service fcdocconverter start 2>&1 | write_log
}

solr_installer(){
	sudo mkdir -p /usr/local/src/solrinstall 2>&1 | write_log
	SOLR_URL="http://archive.apache.org/dist/lucene/solr/5.3.1/solr-5.3.1.tgz"
	cd /usr/local/src/solrinstall 2>&1 | write_log
	sudo wget -N "$SOLR_URL" 2>&1 | write_log
	sudo tar --wildcards -xzvf /usr/local/src/solrinstal/solr-5.3.1.tgz /usr/local/src/solrinstal/solr-5.3.1/bin/install_solr_service.sh 2>&1 | write_log
	sudo cp /usr/local/src/solrinstall/solr-5.3.1/bin/install_solr_service.sh /usr/local/src/solrinstall/ 2>&1 | write_log
	sudo bash /usr/local/src/solrinstall/install_solr_service.sh /usr/local/src/solrinstall/solr-5.3.1.tgz 2>&1 | write_log

	sudo update-rc.d solr defaults 2>&1 | write_log
	sudo service solr restart 2>&1 | write_log

	sudo mkdir -p /var/solr/data/fccore | write_log > /dev/null
	sudo rsync -r /var/www/html/thirdparty/solarium/fcskel/ /var/solr/data/fccore/  | write_log > /dev/null
	sudo chown solr.solr /var/solr/data/fccore -Rf | write_log > /dev/null
}

new_install(){
 	sudo a2enmod headers | write_log > /dev/null
 	sudo service apache2 restart | write_log > /dev/null
 	sudo chmod +x /var/www/html/thirdparty/prop/p23l | write_log > /dev/null
}

binary_file(){
	BINARY_URL="http://patch.codelathe.com/tonidocloud/live/installer/filecloudcp"
	cd /usr/bin | write_log > /dev/nul
	sudo wget -N $BINARY_URL | write_log > /dev/nul
	sudo chmod 744 /usr/bin/filecloudcp | write_log > /dev/null
}


setupCron(){
	sudo echo "*/5 * * * * php /var/www/html/core/framework/cron.php" | sudo crontab -u www-data -  2>/dev/null | write_log > /dev/null
	sudo crontab -u www-data -l | write_log > /dev/null
	sudo chmod +x /var/www/html/thirdparty/prop/* | write_log > /dev/null
}


ubuntu_package_dependency_installer
installFileCloud
new_install
solr_installer
docconvertor_installer
binary_file
setupCron