<?php
/* Configuration values for ClamAV Scanning*/
/* ClamAV must be setup in Daemon Mode running on TCP */

define("TONIDOCLOUD_CLAMAV_ENABLE", 0 );         // < Whether to enabe File Scan
define("TONIDOCLOUD_CLAMAV_HOST", "localhost" ); // < ClamAV Daemon Host
define("TONIDOCLOUD_CLAMAV_PORT", 3310 );        // < ClamAV Daemon port
define("TONIDOCLOUD_CLAMAV_MAXFILESIZE", 25048576 ); // < Only files less than this size in bytes will be scanned

// Advanced
define("TONIDOCLOUD_CLAMAV_STREAM_CHUNK_SIZE", 8192 ); 

?>