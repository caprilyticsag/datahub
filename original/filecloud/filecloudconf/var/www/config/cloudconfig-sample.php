<?php
/* Configuration values for FileCloud */

// ... Server URL
define("TONIDOCLOUD_SERVICENAME", "FileCloud" ); // < VALID name of the service
define("TONIDOCLOUD_SERVER_URL", "http://127.0.0.1" ); // < VALID values are http://HOST:PORT

// ... Cloud Database
define("TONIDOCLOUD_DBSERVER", "mongodb://localhost:27017");
// ... Audit Database
define("TONIDOCLOUD_AUDIT_DBSERVER", "mongodb://localhost:27017");

// ... TimeZone
define("TONIDOCLOUD_DEFAULT_TIMEZONE", "America/Chicago" ); // < VALID values are http://www.php.net/manual/en/timezones.php

// ... Default Storage Size Per user
define("TONIDOCLOUD_DEFAULT_SIZE_PERUSER", 2147483648); // < VALID values are specified in Bytes, 0 means no limit

// =============================================================================
// ... Email Settings
define("TONIDOCLOUD_FROM_EMAIL",          "demo@filecloudmail.com" ); 
define("TONIDOCLOUD_FROM_EMAIL_NAME",     "FileCloud" ); 
define("TONIDOCLOUD_REPLY_TO_EMAIL",      "demo@filecloudmail.com" ); 
define("TONIDOCLOUD_REPLY_TO_EMAIL_NAME", "FileCloud" ); 

define("TONIDOCLOUD_USE_EMAIL", "smtp" ); // < VALID values are mail, smtp, sendmail

// ... if EMAIL is smtp, then we need valid settings
define("TONIDOCLOUD_SMTP_HOSTNAME", "smtpcorp.com" ); // < VALID HOSTNAME for SMTP
define("TONIDOCLOUD_SMTP_PORT", 2525  );                      // < VALID PORTNAME for SMTP
define("TONIDOCLOUD_SMTP_AUTH", true );                    // < VALID values are true, false

// ... If EMAIL is smtp and is set to smtp AUTH to true, then we need these
define("TONIDOCLOUD_SMTP_AUTH_USER", "fcdemo182" );     
define("TONIDOCLOUD_SMTP_AUTH_PASSWORD", "bXo5dnBrdWgyZmUw" );  

define("TONIDOCLOUD_SMTP_SECURE", '' );                    // < VALID values are '', 'ssl', 'tls'
// =============================================================================

// ... Advanced Settings
define("TONIDOCLOUD_OP_MODE", "PROD" ); // < VALID values are DEV | PROD

//... DEFAULT ACCOUNT STATUS 0 - No Automatic Approval, Admin has to approve account, 1 - Automatically Approved To Full User
//... 2 - Automatically Approved To Guest User
define("TONIDOCLOUD_AUTOMATIC_ACCOUNT_APPROVAL", 1); // Whether admin approval is required for creating accounts or not

// ... Flag to send email to admin when a new account is signedup and waiting for approval
// ... 0 - Do not send email, 1 - send email
define("TONIDOCLOUD_WAITINGFORAPPROVAL_EMAIL_TO_ADMIN", 1);

//... Default number of days the user login cookie information will be saved.
define("TONIDOCLOUD_SESSION_TIMEOUT_IN_DAYS", 1);

// ... Admin Username and Password
define("TONIDOCLOUD_ADMIN_USER", "admin");
define("TONIDOCLOUD_ADMIN_PASSWORD", "password");
define("TONIDOCLOUD_API_KEY", "apipassword");

define("TONIDOCLOUD_AUTH", "DEFAULT"); // Other option is ACTIVEDIRECTORY, LDAP

// ... 1 - will check for updates on remote tonido server
// ... 0 - will not check for updates
define("TONIDOCLOUD_CHECK_FOR_UPDATES", 1);

// ... Contents from this folder will be initially loaded into user account
// ... Leave blank if no samples are to be loaded
define("TONIDOCLOUD_SAMPLES_FOLDER","");

// ... Folder used by FileCloud for storing temporary files ex. temp files created during upgrade
// ... Use something like '/tmp' in Linux, 'C:\\temp' in Windows
// ... The specified temporary directory should have read/write permissions for apache process
define("TONIDOCLOUD_TEMP_FOLDER","");

// ... Override URL for FileCloud to download updates from. Default is built-into the code.
define("TONIDOCLOUD_UPDATE_URL_OVERRIDE","");
// =============================================================================
// ... Network Shares
// ... Name of Network Shares
define("TONIDOCLOUD_NETWORK_SHARES_NAME","Network Shares");
// ... Whether Network Shares can be shared
define("TONIDOCLOUD_ALLOW_NETWORKSHARE_SHARING", 1);

//... Audit log level Acceptable values are
//... OFF - No Audit Log Recorded
//... REQUEST - Log all requests and results of request but not the full response
//... FULL - Log complete request and response.
define("TONIDOCLOUD_AUDIT_LOG_LEVEL", "REQUEST");


// =============================================================================
// ... Multi-site Configurations
// ... id - specify unique site id for multisite configurations, 
// ....empty - for single site configuration
define("TONIDOCLOUD_SITE_ID","");

// ... Support Recycle Bin possible values 1 and 0
define("TONIDOCLOUD_RECYCLEBIN","1");

// ... Support WebDAV Access, possible values 1 and 0
define("TONIDOCLOUD_ALLOW_WEBDAV","1");

// ... Skip Recycle bin for files greater than this size (default is 100 MB)
define("TONIDOCLOUD_RECYCLEBIN_SKIP_SIZE", 104857600);

// ... Set this to 0 if non RMC mobile clients are allowed to connect
define("TONIDOCLOUD_RMC_ALLOW_DOWNLEVEL_MOBILE_CLIENTS",1);

// ... Email file change notifications by default. Must have the cron job set up.
define("TONIDOCLOUD_FILE_CHANGE_NOTIFICATIONS", 1);

// ... Allow wildcard search during user search
define("TONIDOCLOUD_USER_WILDCARD_SEARCH", 1);   

// ... OO integration
// LINUX    /opt/openoffice4/program
// WINDOWS  C:\\Program Files (x86)\\openoffice 4\\program
define("TONIDOCLOUD_LOCAL_OOPATH", "");

// Minimum Password strength to
define("TONIDO_MIN_PASSWORD_STRENGTH", 8);

//Enable strong passwords, values 1 and 0
//ie., contain at least (1) upper case letter, (1) lower case letter, 
//(1) number or special character
define("TONIDO_STRONG_PASSWORD_CHECKS", 0);

// Storage implementation 'local' or 'openstack' or 'amazons3'
define("TONIDOCLOUD_STORAGE_IMPLEMENTATION", "local");

// Memcache daemon settings
define("TONIDOCLOUD_MEMCACHED_SERVER", "127.0.0.1");
define("TONIDOCLOUD_MEMCACHED_PORT", 11211);

//define("TONIDOCLOUD_NODE_COMMON_TEMP_FOLDER", "/mnt/common");

?>