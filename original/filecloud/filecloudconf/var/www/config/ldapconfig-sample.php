<?php
/* Configuration values for LDAP Authentication */

// ... Server URL
define("TONIDOCLOUD_LDAP_HOST", "192.168.0.119" ); // < VALID name of LDAP Host for TLS you can specify ldaps://host
define("TONIDOCLOUD_LDAP_PORT", 389 ); // < LDAP port
define("TONIDOCLOUD_LDAP_USERDNTEMPLATE", "uid=^NAME^,ou=People,dc=maxcrc,dc=com"); // < Connection template for user auth
define("TONIDOCLOUD_LDAP_SEARCHDN", "ou=People,dc=maxcrc,dc=com"); // < User DN 
define("TONIDOCLOUD_LDAP_USERFILTERTEMPLATE", "(&(objectClass=inetOrgPerson)(uid=^NAME^))"); // < Search Template for Username
define("TONIDOCLOUD_LDAP_MAILATTRIBUTE", "mail"); // < Email Template

?>
