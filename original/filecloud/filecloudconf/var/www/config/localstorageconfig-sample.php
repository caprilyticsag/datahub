<?php

/******************************************************************************* 
  Copyright(c) 2012 CodeLathe LLC. All rights Reserved.
  This file is part of TonidoCloud  http://www.tonido.com
 *******************************************************************************/

define("TONIDO_LOCALSTORAGE_TOPLEVELFOLDER", "" ); // VALID FS path (eg.WINDOWS 'c:\\tonidocloud\\data' or LINUX '/opt/tonidocloud/data')

define("TONIDO_LOCALSTORAGE_DBSERVER", "mongodb://localhost:27017");
define("TONIDO_LOCALSTORAGE_DBNAME", "tonidostoragedb");

define("TONIDO_LOCALSTORAGE_FILESPERCONTAINER", 5000 );
define("TONIDO_LOCALSTORAGE_FOLDERSPERCONTAINER", 5000 );
define("TONIDO_LOCALSTORAGE_NOOFVERSIONS", 3 );

?>