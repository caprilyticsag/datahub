<?php
/* Configuration values for ICAP/AV Scanning*/

define("TONIDOCLOUD_ICAPAV_ENABLE", 1);  // < Whether to enable ICAP/AV File Scan
define("TONIDOCLOUD_ICAPAV_LOCAL_HOST", "10.0.0.6"); // < FC Server Private IP address - must not be 127.0.0.1 or localhost
define("TONIDOCLOUD_ICAPAV_REMOTE_HOST", "10.0.0.8"); // < ICAP/AV Server IP address
define("TONIDOCLOUD_ICAPAV_PORT", 1344); // < ICAP/AV Server listen port
define("TONIDOCLOUD_ICAPAV_MAXFILESIZE", 25048576); // < Only files less than this size in bytes will be scanned
define("TONIDOCLOUD_ICAPAV_SERVICENAME", "SYMCScanReq-AV"); // < Service name (must be specified after consulting AV documentation)

?>