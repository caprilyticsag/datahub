<?php

/******************************************************************************* 
  Copyright(c) 2012 CodeLathe LLC. All rights Reserved.
  This file is part of TonidoCloud  http://www.tonido.com
 *******************************************************************************/
//Open stack server version
define("TONIDOCLOUD_OSSTORAGE_API_VERSION", "v1.0");

//Open stack server IP or address
define("TONIDOCLOUD_OSSTORAGE_SERVER_HOST", "192.168.1.1");

//Open stack server port
define("TONIDOCLOUD_OSSTORAGE_SERVER_PORT", "8080");

//Open stack account
define("TONIDOCLOUD_OSSTORAGE_ACCOUNT", "test");

//Open stack user
define("TONIDOCLOUD_OSSTORAGE_USER", "tester");

//Open stack password
define("TONIDOCLOUD_OSSTORAGE_PASSWORD", "testing");

//Database name
define("TONIDOCLOUD_OSSTORAGE_DBSERVER", "mongodb://localhost:27017");

//Database details
define("TONIDOCLOUD_OSSTORAGE_DBNAME", "tonidoosstoragedb");

//Files Per Container
define("TONIDOCLOUD_OSSTORAGE_FILESPERCONTAINER", 5000 );

//Number of versions
define("TONIDOCLOUD_OSSTORAGE_NOOFVERSIONS", 3 );
?>