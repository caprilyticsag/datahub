<?php
/* Configuration values for FileCloud */

// ... Server URL
define("BACKUPAGENT_ACCESS_KEY", "backuppassword" ); 

//Additional mongodump params
//Use this if you have additional params to be passed to mongodump command
//define("BACKUPAGENT_MONGODUMP_PARAMS", "--host \"127.0.0.1\" --user mongouser --password passw0rd" ); 
?>