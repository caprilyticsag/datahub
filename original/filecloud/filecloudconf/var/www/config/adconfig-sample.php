<?php
/* Configuration values for ActiveDirectory Authentication */


define("TONIDOCLOUD_AD_HOST", "mydomain.com" ); // < ActiveDirectory Host
define("TONIDOCLOUD_AD_PORT", 389 ); // < ActiveDirectory port
define("TONIDOCLOUD_AD_ACCOUNTSUFFIX", "@tonidocloud.local"); // < User Login Name Suffix
define("TONIDOCLOUD_AD_BASEDN", "DC=tonidocloud,DC=local"); // < User Search DN 
define("TONIDOCLOUD_AD_MAILATTRIBUTE", "mail"); // < Mail Attribute
define("TONIDOCLOUD_AD_LIMIT_GROUP", ""); // < If you want login users to be limited to a specific AD group
define("TONIDOCLOUD_AD_USETLS", false); //<< If you want to use TLS set true, default is false, both SSL and TLS can't be true
define("TONIDOCLOUD_AD_USESSL", false); //<< If you want to use SSL set true, default is false, both SSL and TLS can't be true

define("TONIDOCLOUD_AD_ACCOUNTNAME", ""); // < Account name for Admin Operations 
define("TONIDOCLOUD_AD_ACCOUNTPASSWORD", ""); // < Account Password for Admin Operations

define("TONIDOCLOUD_AD_USEADMINBINDING", "0"); // < Account name for Admin Operations 
define("TONIDOCLOUD_AD_ADMINACCOUNTNAME", ""); // < AD Service Account Username
define("TONIDOCLOUD_AD_ADMINACCOUNTPASSWORD", ""); // < AD Service Account Password

// ... Multi-AD Server Support
define("TONIDOCLOUD_MULTI_AD_ENABLE", 0);

?>