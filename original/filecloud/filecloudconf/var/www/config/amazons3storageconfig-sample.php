<?php

/******************************************************************************* 
  Copyright(c) 2012 CodeLathe LLC. All rights Reserved.
  This file is part of TonidoCloud  http://www.tonido.com
 *******************************************************************************/

//------------ NOTE NOTE NOTE NOTE ------------------------------------------
// Optional override keys. Not typically  used.

define ("TONIDOCLOUD_DISABLE_S3_REDIRECT", "0");

define ("TONIDOCLOUD_S3_REDUCED_REDUNDANCY", "0");

define ("TONIDOCLOUD_S3_DL_SIZE_LIMIT_BYTES", "0");

//define ("TONIDOCLOUD_S3_PROXY","username:password@proxyip.com:port");

?>