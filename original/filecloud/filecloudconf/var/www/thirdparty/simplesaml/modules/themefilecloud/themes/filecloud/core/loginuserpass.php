<?php
    header("HTTP/1.0 404 Not Found");  
	header('Content-type: text/html;');
 ?>
        <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>FileCloud Page Not Found</title>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
<!-- 
NOTE:
- There's a long line of spaces above this note. That's because iOS email clients don't download POP emails if the head is shorter than 1,019 characters
- All images have style="display:block". This forces Gmail to display the table cells correctly
- style="font-size:1px" is needed in empty table cells smaller than 15 pixels. Outlook 2013 adds a 15px whitespace by default to these cells, unless there is a font-size set to 1px
-->
	
</head>
<body style="margin:0px; padding:0px; background:#ffffff">
    <table width="480" cellpadding="0" cellspacing="0" border="0" align="center" style="margin: auto">
        <tr>
            <td colspan="3"><img src="/core/getcustomimage?type=login" alt="fileCloud" style="display:block"></td>
        </tr>
        <tr>
            <td style="background: #1265a0;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="background: #1265a0; color: #ffffff"><br>
                <h2 style="font-family: Arial, Helvetica; font-size: 25px; font-weight: normal; padding: 0px; margin: 0px">FileCloud Page Not Found!</h2>
                <p style="font-family: Arial, Helvetica; font-size: 14px; padding: 0px; margin: 0px">Sorry, we couldn't find the page you are looking for.</p>
                <br>
            </td>
            <td style="background: #1265a0;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>        
        </tr>
        <tr>
            <td style="background: #f7f7f7;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>    
            <td style="background: #f7f7f7; font-family: Arial, Helvetica; font-size: 14px; line-height: 20px; text-align:left"><br>
				<p>Please check the URL for proper spelling and capitalization.</p>
                <p>You can return back to the  <a href="/">FileCloud Home Page</a></p>
                <br>
            </td>
            <td style="background: #f7f7f7;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>            
        </tr>
        <tr>
            <td colspan="3" style="text-align:center"><br>

                <br>
            </td>
        </tr>
    </table>
</body>
</html>