<?php

/* **********************************************************************
 * INSTRUCTIONS TO UPDATE SIMPLESAML MODULE
 * **********************************************************************
 * Gets the Configuration parameters for Config file inside simplesaml.
 * When SimpleSAML is updated, this file must be copied.
 * 
 * Custom Files and Modified Files inside SimpleSAML.
 * 
 * 1. /config/config.php (modified)
 * 2. /config/authsources.php (modified)
 * 3. /config/filecloudconfig.php (New)
 * 4. /lib/metadata/metadatastoragehandlerxmlstring.php (new)
 * 5. /lib/metadata/metadatastoragesource.php (moified)
 */

/*
 * Add the Proxy Server Information here
 * Format is as follows user:password@proxyserverurl.com
 */

define("TONIDOCLOUD_SAML_PROXY", "");

/*
 * Get the SamlConfiguration from the mongodb
 */
function getFileCloudConfig() {        
    
        $result = array ('idpurl' => '', 'contactemail' => '', 'contactname' => '', 'adfs' => '', 'logmode' => '');
    
        $xmlresponse = getCurlResponse("getsamlconfig");
        
        if ($xmlresponse == null)
        {
            SimpleSAML_Logger::error(__FUNCTION__ . " Exception: Unable to read from /url/core/getsamlconfig data");
            return $result;
        }
        
        $xml = simplexml_load_string($xmlresponse);     
        
        if (!$xml)
        {
            SimpleSAML_Logger::error(__FUNCTION__ . " Exception: Invalid xml format from /url/core/getsamlconfig data");
            return $result;
        }       
                
        if (isset($xml->item[0]->idpurl))
        {
            $result['idpurl'] = (string)$xml->item[0]->idpurl;
        }
        
        if (isset($xml->item[0]->contactemail))
        {
            $result['contactemail'] = (string)$xml->item[0]->contactemail;
        }
        
        if (isset($xml->item[0]->contactname))
        {
            $result['contactname'] = (string)$xml->item[0]->contactname;
        }
        
        if (isset($xml->item[0]->adfs))
        {
            $result['adfs'] = (string)$xml->item[0]->adfs;
        }
        
        if (isset($xml->item[0]->logmode))
        {
            $result['logmode'] = (string)$xml->item[0]->logmode;
        }       
      
        
        SimpleSAML_Logger::info(print_r($result,TRUE));    
       
        return  $result;        
}


/*
 * Get the curl response for metadata or samlconfig etc.
 */
function getCurlResponse($command){
    
      
        SimpleSAML_Logger::info(__FUNCTION__ . " Attempting Curl Command $command in SimpleSaml");
      

        $hostName = "";
        $protocol = "http";
        $proxyurl = "";
        $proxyauth = "";

        if( isset($_SERVER['HTTPS']))
        {
            $protocol = "https";
        }
        if (isset($_SERVER['HTTP_HOST']))
        {
            $hostName = $_SERVER['HTTP_HOST'];
        }

        $url = $protocol . '://' . $hostName . "/core/" . $command;

        if (defined("TONIDOCLOUD_SAML_PROXY"))
        {
            if (TONIDOCLOUD_SAML_PROXY !== "")
            {
                SimpleSAML_Logger::info(__FUNCTION__ . " Proxy enabled for SimpleSAML");
                
                $protected = strpos(TONIDOCLOUD_SAML_PROXY, '@');
                if ($protected === false)
                {
                    $proxyurl = TONIDOCLOUD_SAML_PROXY;
                }
                else
                {
                    $exploded = explode('@', TONIDOCLOUD_SAML_PROXY);
                    $proxyauth = $exploded[0];
                    $proxyurl = $exploded[1];
                }
            }
        }

        // initialize curl
        $curl = curl_init();
        
        curl_setopt($curl, CURLOPT_URL, $url);        
        
        //proxy settings if applicable
        if ($proxyurl != "")
        {
           curl_setopt($curl, CURLOPT_PROXY, $proxyurl);
           if ($proxyauth != "")
           {
               curl_setopt($curl, CURLOPT_PROXYUSERPWD, $proxyauth);
           }
        }
        
        // return the curl response to be captured in a variable.
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);     
        
        // wait max of 10 seconds for connection to be established
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,10); 
        
        // We dont care about https here because no secret data is transfered here.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        
        // get the httpcode for later verification
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);        
           
                
        if(!$response = curl_exec ($curl))
        {
            // echo results alone works. Fails to log data here.
            echo "SAML Curl Request Failed.  HTTP code: $httpCode. Curl ERR: " . curl_error($curl) . " [Curl Error no " . curl_errno($curl) . "]";
            exit(1);
        } 
        

        curl_close($curl);       
        
    
        return $response;
}