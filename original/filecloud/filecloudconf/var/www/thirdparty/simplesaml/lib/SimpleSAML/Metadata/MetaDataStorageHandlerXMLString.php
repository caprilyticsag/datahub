<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SimpleSAML_MetaDataStorageHandlerXMLString
 *
 * @author sathya
 */
class SimpleSAML_Metadata_MetaDataStorageHandlerXMLString extends SimpleSAML_Metadata_MetaDataStorageSource {
    /**
	 * This variable contains an associative array with the parsed metadata.
	 */
	private $metadata;
        
        private $xmlvalue;


	/**
	 * This function initializes the XML metadata source. The configuration must contain one of
	 * the following options:
	 * - 'xmlvalue': string in xml format.	 	 
	 *
	 * @param $config  The configuration for this instance of the XML metadata source.
	 */
	protected function __construct($config) {	          
              

		if(array_key_exists('xmlvalue', $config)) {
			$this->xmlvalue = $config['xmlvalue'];		
		} else {
                        SimpleSAML_Logger::ERR("Missing Idp Metadata xml string value in configuration.");
			throw new Exception('Missing either xml string value in XML metadata source configuration.');
		}

		$SP1x = array();
		$IdP1x = array();
		$SP20 = array();
		$IdP20 = array();
		$AAD = array();

		$entities = $this->parseDescriptorsXmlString();
		foreach($entities as $entityId => $entity) {

			$md = $entity->getMetadata1xSP();
			if($md !== NULL) {
				$SP1x[$entityId] = $md;
			}

			$md = $entity->getMetadata1xIdP();
			if($md !== NULL) {
				$IdP1x[$entityId] = $md;
			}

			$md = $entity->getMetadata20SP();
			if($md !== NULL) {
				$SP20[$entityId] = $md;
			}

			$md = $entity->getMetadata20IdP();
			if($md !== NULL) {
				$IdP20[$entityId] = $md;
			}

			$md = $entity->getAttributeAuthorities();
			if (count($md) > 0) {
				$AAD[$entityId] = $md[0];
			}
		}

		$this->metadata = array(
			'shib13-sp-remote' => $SP1x,
			'shib13-idp-remote' => $IdP1x,
			'saml20-sp-remote' => $SP20,
			'saml20-idp-remote' => $IdP20,
			'attributeauthority-remote' => $AAD,
			);

	}


	/**
	 * This function returns an associative array with metadata for all entities in the given set. The
	 * key of the array is the entity id.
	 *
	 * @param $set  The set we want to list metadata for.
	 * @return An associative array with all entities in the given set.
	 */
	public function getMetadataSet($set) {
		if(array_key_exists($set, $this->metadata)) {
			return $this->metadata[$set];
		}

		/* We don't have this metadata set. */
		return array();
	}
        
        
               
        
        private function parseDescriptorsXmlString() {      
            
            $data = $this->xmlvalue;   
            
            $doc = new DOMDocument();
            
            $res = $doc->loadXML($data);
            
            if($res !== TRUE) {
                    SimpleSAML_Logger::ERR("Failed to read Idp metadata xml");
                    throw new Exception('Failed to read Idp Metadata XML String');
            }
            
            if ($doc->documentElement === NULL) {
                SimpleSAML_Logger::ERR("Idp metadata is not a valid xml document");
                throw new Exception('String is not a valid XML document');
            }
            
            SimpleSAML_Logger::info("Idp Metadata is valid xml format");

            return SimpleSAML_Metadata_SAMLParser::parseDescriptorsElement($doc->documentElement);
	}      

}
