var switchtoromode = function(){
	$('head').append("<style type='text/css'  media='print'> body { visibility: hidden; display:none; }</style>");
	$('#print').hide();
	$('#secondaryPrint').hide();
	$('#download').hide();
	$('#secondaryDownload').hide();
	$('#viewBookmark').hide();
	
	PDFJS.disableTextLayer = true;
	
}
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substr(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var onLoad = function(){
	var windowname = self.window.name;
	var framename = "";
	var userAgent = window.navigator.userAgent.toLowerCase(), mobile = /mobile/.test( userAgent );
	if(self.window.frameElement != null){
		framename = self.window.frameElement.getAttribute("name");
	}
	if( windowname == "readonly" || framename == "readonly" || mobile ){
		switchtoromode();
	}		
}

var onPageInit = function(){
	//Don't show sidebar by default
	PDFViewerApplication.viewerPrefs['showPreviousViewOnLoad'] = false;
	PDFViewerApplication.viewerPrefs['disablePageMode'] = true;
	//open all links in new window
	PDFJS.externalLinkTarget = PDFJS.LinkTarget.BLANK
}

var onPageLoad = function(){
	//set title
	var filepath = getUrlParameter('name');
	this.title=decodeURIComponent(filepath);
}
document.addEventListener('DOMContentLoaded', onLoad, true);
document.addEventListener("pagesinit", onPageInit, true);
document.addEventListener("pagesloaded", onPageLoad, true);