<?php

/******************************************************************************* 
  Copyright(c) 2012 CodeLathe LLC. All rights Reserved.
  This file is part of TonidoCloud  http://www.tonido.com
 *******************************************************************************/

namespace core;

// ... Define TONIDO_CLOUD_ROOT_DIR Define for use by other code
define('TONIDO_CLOUD_ROOT_DIR', realpath(__DIR__ . '/..'));

// ... Include all the common stuff needed for running our system
require_once(TONIDO_CLOUD_ROOT_DIR . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'framework' . DIRECTORY_SEPARATOR . 'runtime.php');

// ... Start our Main Handler
\core\framework\TonidoCloudServer::getInstance()->execute();

?>