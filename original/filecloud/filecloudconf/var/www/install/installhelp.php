<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>FileCloud Basic Checks</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="apple-touch-icon" sizes="57x57" href="images/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/fav/favicon-16x16.png">
    <link rel="manifest" href="images/fav/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="images/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="additional_styles.css">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,700,600" rel="stylesheet" type="text/css">
</head>


<body>

    <div class="container-fluid">

        <nav>
            <div class="nav-content">
                <a class="nav-logo" href="index.html"><img src="images/logo.svg" alt="FileCLoud" /></a>
                <div class="nav-contact-blurb">
                    If you face any issues or have any questions, please contact us at <a href="mailto:support@codelathe.com">support@codelathe.com</a>
                </div>
            </div>

        </nav>


        <div class="page-content">

            <section class="masthead">
                <div class="masthead-content">
                  <h1><img src="images/check_shield.svg" />Installation Checks</h1>
                  <h2>Getting Started</h2>

                  <section class="masthead-boxes row">
                      <div class="box col-sm-3"><a href="https://www.getfilecloud.com/supportdocs/display/cloud/Introduction" class="box-content" target="_blank"><img src="images/mast_icon1.svg" /><span>FileCloud Introduction</span></a></div>
                      <div class="box col-sm-3"><a href="https://www.getfilecloud.com/supportdocs/display/cloud/Requirements" class="box-content" target="_blank"><img src="images/mast_icon3.svg" /><span>Installation Requirements</span></a></div>
                      <div class="box col-sm-3"><a href="https://www.getfilecloud.com/support/display/cloud/Getting+Started" class="box-content" target="_blank"><img src="images/mast_icon2.svg" /><span>Getting Started</span></a></div>
                      <div class="box col-sm-3"><a href="https://www.getfilecloud.com/support" class="box-content" target="_blank"><img src="images/mast_icon4.svg" /><span>Documentation</span></a></div>
                  </section>

                </div>
            </section>

            <section class="verify" id="extended-checks">
                <div class="section-content">

                    
                    <div class="check-tabs">
                        <ul>
                            <li><a href="index.php">Basic Checks</a></li>
                            <li><a href="index.php?extended=1">Extended Checks</a></li>
                        </ul>
                    </div>
                </div>
            </section>

            <section class="help">
                <div class="section-content">
                        <i class="help-icon fa fa-question-circle" aria-hidden="true"></i>
                        <h2>FileCloud Installation Help</h2>
                        <h3 class="subtitle">This page provides detailed information on installation help with FileCloud.</h3>

                        <h3><a name="apache">Apache Server</a></h3>
                        <p>A working Apache Server installation is required for FileCloud to function. Apache can be installed and run on Windows or Linux(Recommended).</p>

                        <h3><a name="rewrite">Mod Rewrite</a></h3>
                        <p>A working Apache mod_rewrite extension is required for FileCloud to function. FileCloud uses URL redirection and rewriting extensively, so mod_rewrite will be required.</p>

                        <h3><a name="php53">PHP 5.6</a></h3>
                        <p>FileCloud uses latest PHP language features, so atleast PHP 5.6 is required.</p>

                        <h3><a name="mongo">Mongo DB</a></h3>
                        <p>FileCloud uses a nosql database called mongo db for high performance and scalability.</p>

                        <h3><a name="gd">GD</a></h3>
                        <p>FileCloud uses PHP GD extensions for image resizing etc.</p>

                        <h3><a name="zip">Zip</a></h3>
                        <p>FileCloud uses PHP Zip extensions for extracting zip archives</p>

                        <h3><a name="curl">CURL</a></h3>
                        <p>FileCloud uses PHP CURL extension for Open Stack Swift support etc.</p>

                        <h3><a name="openssl">OpenSSL</a></h3>
                        <p>FileCloud uses PHP OpenSSL extensions for License management</p>

                        <h3><a name="ldap">LDAP</a></h3>
                        <p>FileCloud uses PHP LDAP extensions for connecting to LDAP/Active Directory. This is optional and only required if you are using LDAP or Active Directory authentication.</p>

                        <h3><a name="memcache">Memcache</a></h3>
                        <p>FileCloud uses Memcache which is a high-performance, distributed memory object caching system for File Encryption and for improving performance.</p>

                        <h3><a name="simplexml">SimpleXML</a></h3>
                        <p>FileCloud uses PHP SimpleXML extensions to parse XML. </p>

                        <h3><a name="bcmath">bcMath</a></h3>
                        <p>FileCloud uses PHP bcMath extensions for large computations. </p>

                        <h3><a name="mbstring">mbstring</a></h3>
                        <p>FileCloud uses PHP mbstring extensions for processing multi-byte characters. </p>

                        <h3><a name="serverroot">Server Root</a></h3>
                        <p>FileCloud needs to be installed in the webserver's root folder. ie: The FileCloud install has to be in the main URL, say http://www.cloud.com/install or say http://subdomain.cloud.com/install,
                            it cannot be in the subfolder like http://www.cloud.com/subfolder/install </p>

                        <h3><a name="cloudconfig">Cloud Config File</a></h3>
                        <p>FileCloud stores configuration files in a file called cloudconfig.php under WWWROOT/config directory.
                        A sample config file called cloudconfig-sample.php is provided. You can rename this file to cloudconfig.php.
                        A full explanation of the parameters in cloudconfig is provided at <a href="http://www.tonido.com/support/display/cloud/cloudconfig.php">http://www.tonido.com/support/display/cloud/cloudconfig.php</a></p>

                        <h3><a name="localstorageconfig">Local Storage Config File</a></h3>
                        <p>You can specify where FileCloud should store files and data by changing the Storage Path setting in the Admin Portal (Settings-&gt;Storage-&gt;Managed Storage Settings). </p>
                        <p>You can also change the parameter in a file called localstorageconfig.php under WWWROOT/config directory.
                        A sample config file called localstorageconfig-sample.php is provided. You can rename this file to localstorageconfig.php.
                        A full explanation of the parameters in localstorageconfig is provided at <a href="http://www.tonido.com/support/display/cloud/localstorageconfig.php">http://www.tonido.com/support/display/cloud/localstorageconfig.php</a></p>

                        <h3><a name="openstackstorageconfig">Open Stack Config File</a></h3>
                        <p>When using Open Stack Swift storage as storage backend, you need to have a file called openstackstorageconfig.php under WWWROOT/config directory.
                        A sample config file called openstackstorageconfig-sample.php is provided. You can rename this file to openstackstorageconfig.php.
                        </p>

                        <h3><a name="amazons3storageconfig">Amazon S3 Config File</a></h3>
                        <p>When using Amazon S3 as storage backend, you need to have a file called amazons3storageconfig.php under WWWROOT/config directory.
                        A sample config file called amazons3storageconfig-sample.php is provided. You can rename this file to amazons3storageconfig.php.
                        </p>

                        <h3><a name="scratchpath">Scratch Folder</a></h3>
                        <p>FileCloud writes log files into the scratch folder at WWWROOT/scratch. It needs to be writable by the WebServer.</p>

                        <h3><a name="configpath">Config Folder</a></h3>
                        <p>FileCloud reads configuration files at WWWROOT/config folder. It needs to be readable by the WebServer.</p>

                        <h3><a name="localstoragepath">Local Storage Folder</a></h3>
                        <p>You can specify where FileCloud should store files and data by changing the Storage Path setting in the Admin Portal (Settings-&gt;Storage-&gt;Managed Storage Settings). </p>
                        <p>FileCloud needs the Local Storage folder specified in localstorageconfig.php file (TONIDO_LOCALSTORAGE_TOPLEVELFOLDER) is readable and writable by the webserver.
                        </p>

                        <h3><a name="license">License</a></h3>
                        <p>FileCloud needs the License file (license.xml) at WWWROOT/config/license.xml.
                        </p>

                        <h3><a name="adminpasswd">Admin Password</a></h3>
                        <p>The default admin password needs to be changed from the default. Change the password by editing the (TONIDOCLOUD_ADMIN_PASSWORD) at WWWROOT/config/cloudconfig.php.
                        </p>

                        <h3><a name="adminemail">Admin Email</a></h3>
                        <p>The default admin email needs to be changed from the default. Change the email by editing the (TONIDOCLOUD_REPLY_TO_EMAIL) parameter  at WWWROOT/config/cloudconfig.php.
                        </p>

                        <h3><a name="smtpserver">SMTP Server</a></h3>
                        <p>The default SMTP server needs to be changed from the default demo SMTP server provided. Change the SMTP mail server by editing the mail parameters ( at WWWROOT/config/cloudconfig.php.
                        </p>

                        <h3><a name="email">Email</a></h3>
                        <p>FileCloud needs to be able to send emails, you can modify the email settings in WWWROOT/config/cloudconfig.php.
                        </p>

                        <h3><a name="serverurl">Server URL</a></h3>
                        <p>FileCloud needs the Public domain Name or URL used to access the service. This is used in creating links in email etc. Eg: http://filecloud.xyzcompany.com
                        </p>

                </div>
            </section>





            <footer>
                <div class="section-content">
                    FileCloud &copy; CodeLathe Technologies Inc 2008-2017
                </div>
            </footer>
