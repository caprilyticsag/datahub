<?php
if (isset($_REQUEST['phpinfo']))
{
    phpinfo();
    exit(0);
}

$index = 1;
define('TONIDO_CLOUD_ROOT_DIR', realpath(__DIR__ . '/..'));
if (isset($_REQUEST['extended']))
{
    // ... Include all the common stuff needed for running our system
    require_once(TONIDO_CLOUD_ROOT_DIR . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'framework' . DIRECTORY_SEPARATOR . 'runtime.php');
    require_once(TONIDO_CLOUD_ROOT_DIR . DIRECTORY_SEPARATOR . "admin". DIRECTORY_SEPARATOR ."updatemanager.php");

}
?>

<!DOCTYPE html>
<html lang="en">
<html>
    <head>
        <meta charset="utf-8">
        <title>FileCloud Install</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="apple-touch-icon" sizes="57x57" href="images/fav/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="images/fav/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/fav/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="images/fav/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/fav/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="images/fav/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="images/fav/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="images/fav/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="images/fav/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="images/fav/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="images/fav/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="images/fav/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="images/fav/favicon-16x16.png">
        <link rel="manifest" href="images/fav/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="images/fav/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">


        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="additional_styles.css">
    </head>
    <body>
        
         <div class="container-fluid">

        <nav>
            <div class="nav-content">
                <a class="nav-logo" href="index.html"><img src="images/logo.svg" alt="FileCLoud" /></a>
            </div>

        </nav>

        <section class="help-stripe">
            <div class="contact-blurb">
                <i class="fa fa-question-circle" aria-hidden="true"></i> If you face any issues or have any questions, please contact us at <a href="mailto:support@codelathe.com">support@codelathe.com</a>
            </div>
        </section>

        <div class="page-content">

            <section class="masthead">
                <div class="masthead-content">
                  <h1><img src="images/check_shield.svg" />Installation Checks</h1>
                  <h2>Getting Started</h2>

                  <section class="masthead-boxes row">
                      <div class="box col-sm-3"><a href="https://www.getfilecloud.com/supportdocs/display/cloud/Introduction" class="box-content" target="_blank"><img src="images/mast_icon1.svg" /><span>FileCloud Introduction</span></a></div>
                      <div class="box col-sm-3"><a href="https://www.getfilecloud.com/supportdocs/display/cloud/Requirements" class="box-content" target="_blank"><img src="images/mast_icon3.svg" /><span>Installation Requirements</span></a></div>
                      <div class="box col-sm-3"><a href="https://www.getfilecloud.com/support/display/cloud/Getting+Started" class="box-content" target="_blank"><img src="images/mast_icon2.svg" /><span>Getting Started</span></a></div>
                      <div class="box col-sm-3"><a href="https://www.getfilecloud.com/support" class="box-content" target="_blank"><img src="images/mast_icon4.svg" /><span>Documentation</span></a></div>
                  </section>

                </div>
            </section>

            <?php if (!isset($_REQUEST['extended']))
                     echo "<section class=\"verify\" id=\"basic-checks\">";
                else
                    echo "<section class=\"verify\" id=\"extended-checks\">";
             ?>
                <div class="section-content">

                    <p>Verify your installation by ensuring the following basic and advanced checks pass.</p>
                    <div class="check-tabs">
                        <ul>
                               <?php if (!isset($_REQUEST['extended']))
                                        echo  "<li class=\"active\"><a href=\"index.php\">Basic Checks</a></li>";
                                   else
                                       echo  "<li><a href=\"index.php\">Basic Checks</a></li>";
                                ?>
                                <?php if (!isset($_REQUEST['extended']))
                                        echo  "<li><a href=\"index.php?extended=1\">Extended Checks</a></li>";
                                   else
                                       echo  "<li class=\"active\"><a href=\"index.php?extended=1\">Extended Checks</a></li>";
                                ?>

                            
                        </ul>
                    </div>
                </div>
            </section>
	

        <?php

        class ResultInfo {
               public function __construct($result, $name, $help){
                    $this->m_result = $result;
                    $this->m_help = $help;
                    $this->m_name = $name;
                }

            public $m_name;
            public $m_result;
            public $m_help;
        }

        function apache_header_exists()
	{
		foreach ($_SERVER as $value)
		{
			if (strpos(strtolower($value), 'apache') !== false)
				return true;
		}

		return false;
	}

        function top_htaccess_exists()
	{
             $htaccess_path = realpath('..'.DIRECTORY_SEPARATOR.'.htaccess');
             return is_readable($htaccess_path);
	}


        function apache_mod_rewrite()
	{

		foreach (apache_get_modules() as $value)
		{
			if (strpos(strtolower($value), 'mod_rewrite') !== false)
				return true;
		}

		return false;
	}

        function apacheversion() {
               return $_SERVER['SERVER_SOFTWARE'];
        }

        function check_mod_rewrite()
        {
            $pageURL =  "http://";
            if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')
                $pageURL = "https://";
            if ($_SERVER["SERVER_PORT"] != "80")
            {
                $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].'/core/getauthenticationinfo';
            }
            else
            {
                $pageURL .= $_SERVER["SERVER_NAME"].'/core/getauthenticationinfo';
            }
            
            if (extension_loaded('curl')) {
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $pageURL);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
                curl_setopt($ch, CURLOPT_HTTPGET, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 5); //timeout in seconds
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);               
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                $server_output = curl_exec($ch);
                curl_close($ch);

                if ($server_output !== FALSE) {
                    if (strlen(trim($server_output)) > 0) {
                        libxml_use_internal_errors(true);
                        $xml = simplexml_load_string($server_output);
                        if ($xml !== false) {
                             $OS =  (string)$xml->info->OS;
                             
                             if ($OS === 'TONIDO_CLOUD')
                                return true;
                        }
                    }
                }
            }
            
            
            return false;
            
        }

        function check_path_writable($path)
        {
            //In windows "" is a valid path, but produces unexpected results
            if (!is_dir($path) || $path == "")
                return false;

            try
            {
                $fp = @fopen($path.DIRECTORY_SEPARATOR.'touchfile', 'w');
                if ($fp !== FALSE)
                {
                    $content = "FileCloud Rocks...";
                    if (fwrite($fp, $content, strlen($content)) === FALSE)
                        return false;
                    @fflush($fp);
                    @fclose($fp);

                    $fpr = @fopen($path.DIRECTORY_SEPARATOR.'touchfile', 'r');
                    if ($fpr !== FALSE)
                    {
                        $rdData = @fgets($fpr);
                        if ($rdData === FALSE)
                            return false;
                        @fclose($fpr);

                        if ($rdData === $content)
                        {
                            if (@unlink($path.DIRECTORY_SEPARATOR.'touchfile') === FALSE)
                                    return false;
                            return true;
                        }
                    }
                }
            }
            catch(\Exception $e)
            {
            }

            return false;
        }

        function check_send_email()
        {
             $em = new \core\framework\EmailManager();
             $mail = new \core\framework\Email();

             $mail->setTo(\core\framework\Utility::getAdminEmailAddress(),"Admin");
             $mail->setSubject('FileCloud Email Send Verification');

             $body = "Your FileCloud Emails are being sent OK!";
             $mail->setBody($body);
             return $em->sendEmail($mail);
        }

        function mongodblibrary_check()
        {
            if (!extension_loaded("mongodb"))
                return false;

            return version_compare(phpversion("mongodb"), "1.2.3", ">=");
        }

        function ioncube_check(&$found)
        {
            if (!extension_loaded('ionCube Loader'))
                return false;

            if (function_exists('ioncube_loader_iversion')) {
                    $liv = ioncube_loader_iversion();
                    $lv = sprintf("%d.%d.%d", $liv / 10000, ($liv / 100) % 100, $liv % 100);
                    $found = $lv;

                    $latest_version =  '10.1.0'; // Minimum required
                    $lat_parts = explode('.',$latest_version);
                    $cur_parts = explode('.',$lv);

                    if (($cur_parts[0] > $lat_parts[0]) ||
                        ($cur_parts[0] == $lat_parts[0] && $cur_parts[1] > $lat_parts[1]) ||
                        ($cur_parts[0] == $lat_parts[0] && $cur_parts[1] == $lat_parts[1] && $cur_parts[2] >= $lat_parts[2]))
                    {
                        return true;
                    }
            }

            return false;

        }
        
        function apache_version()
        {
            $ar = preg_split("[/ ]",$_SERVER['SERVER_SOFTWARE']);
            $Apache_Version = "";
            for ($i=0;$i<(count($ar));$i++)
            {
                switch(strtoupper($ar[$i]))
                {
                    case 'APACHE':
                        $i++;
                        $Apache_Version = $ar[$i];
                        break;
                }
            }
            
            if ($Apache_Version == "")
                return "";
            
            return "Version: ".$Apache_Version."<br/>";
        }
        
        function memcacheserver_check(&$version)
        {
            $memcache = \core\framework\MemcacheManager::getInstance()->getConnection();
            if ($memcache !== false)
            {
                $version = $memcache->getversion();
                return true;
            }
            return false;
        }

        function check_requirements()
	{

		$result = array();

                if (!isset($_REQUEST['extended']))
                {
                    $result['Apache Web Server'] = new ResultInfo(function_exists('apache_getenv') || apache_header_exists(), NULL, apache_version()."Running as : ".get_current_user()."<br/><a href='installhelp.php#apache'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    $result['Apache Mod Rewrite'] = new ResultInfo(apache_mod_rewrite(), NULL, "<a href='installhelp.php#rewrite'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    $result['.htaccess present'] = new ResultInfo(top_htaccess_exists(), NULL, "");
                    $result['PHP 7.1'] = new ResultInfo(version_compare(PHP_VERSION , "7.1.0", ">="), NULL, "Version: ".PHP_VERSION ."<br/> <a href='installhelp.php#php53'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a> <a href=\"?phpinfo=1\">PHP Info</a>");
                    $result['PHP MongoDB (mongodb ext) driver 1.2.3 or higher'] = new ResultInfo(mongodblibrary_check(), NULL, "Version: ".phpversion("mongodb")."<br/><a href='installhelp.php#mongo'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    $result['PHP GD library'] = new ResultInfo(extension_loaded('gd'), NULL, "<a href='installhelp.php#gd'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    $result['PHP Zip library'] = new ResultInfo(extension_loaded('zip'), NULL, "<a href='installhelp.php#zip'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    $result['PHP Curl library'] = new ResultInfo(extension_loaded('curl'), NULL, "<a href='installhelp.php#curl'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    $result['PHP OpenSSL library'] = new ResultInfo(function_exists('openssl_open'), NULL, "<a href='installhelp.php#openssl'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i> Notes</a>");
                    $icversion = '';
                    $result['PHP ionCube extension 10.1.0 or higher'] = new ResultInfo(ioncube_check($icversion), NULL, "Version: $icversion<br/><a href='loader-wizard.php'>Install ionCube Loader</a>");
                    $result['PHP bcmath extension'] = new ResultInfo(extension_loaded('bcmath'), NULL, "<a href='installhelp.php#bcmath'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    $result['PHP SimpleXML extension'] = new ResultInfo(extension_loaded('SimpleXML'), NULL, "<a href='installhelp.php#simplexml'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    $result['PHP mbstring extension'] = new ResultInfo(extension_loaded('mbstring'), NULL, "<a href='installhelp.php#mbstring'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    $result['PHP LDAP library (optional, for AD/LDAP support)'] = new ResultInfo(extension_loaded('ldap'), NULL, "Ignore failure if AD/LDAP is not needed <br/><a href='installhelp.php#ldap'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    $result['PHP Memcache Extension (optional)'] = new ResultInfo(extension_loaded("memcache"), NULL, "Version: ".phpversion("memcache")."<br/><a href='installhelp.php#memcache'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    
                    $filePath = realpath(dirname(__FILE__));
                    $rootPath = realpath($_SERVER['DOCUMENT_ROOT']).DIRECTORY_SEPARATOR;
                    $remPath = str_replace($rootPath, '', $filePath);


                    $result['Install in Server WWW root folder'] = new ResultInfo(($remPath === 'install'), NULL, "<a href='installhelp.php#serverroot'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                }

                if (isset($_REQUEST['extended']))
                {
                    $configpath = realpath(TONIDOCLOUD_CONFIG_PATH. DIRECTORY_SEPARATOR .'cloudconfig.php');
                    $result['CloudConfig.php Readable:'.$configpath] = new ResultInfo(is_readable($configpath), NULL, "<a href='installhelp.php#cloudconfig'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");

                    if(defined('TONIDOCLOUD_STORAGE_IMPLEMENTATION') && (TONIDOCLOUD_STORAGE_IMPLEMENTATION == "openstack"))
                    {
                        $configpath = realpath(TONIDOCLOUD_CONFIG_PATH. DIRECTORY_SEPARATOR .'openstackstorageconfig.php');
                        $result['openstackstorageconfig.php Readable:'.$configpath] = new ResultInfo(is_readable($configpath), NULL, "<a href='installhelp.php#openstackstorageconfig'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    }
                    elseif (defined('TONIDOCLOUD_STORAGE_IMPLEMENTATION') && (TONIDOCLOUD_STORAGE_IMPLEMENTATION == "amazons3"))
                    {
                        $configpath = realpath(TONIDOCLOUD_CONFIG_PATH. DIRECTORY_SEPARATOR .'amazons3storageconfig.php');
                        $result['amazons3storageconfig.php Readable:'.$configpath] = new ResultInfo(is_readable($configpath), NULL, "<a href='installhelp.php#amazons3storageconfig'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    }
                    else
                    {
                        $configpath = realpath(TONIDOCLOUD_CONFIG_PATH. DIRECTORY_SEPARATOR .'localstorageconfig.php');
                        $result['localstorageconfig.php Readable:'.$configpath] = new ResultInfo(is_readable($configpath), NULL, "<a href='installhelp.php#localstorageconfig'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    }
                    
                    $install_path = realpath(TONIDOCLOUD_SCRATCH_PATH);
                    $result['Scratch Directory Writable:'.$install_path] = new ResultInfo(is_writable($install_path), NULL, "<a href='installhelp.php#scratchpath'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");

                    $install_path = realpath(TONIDOCLOUD_CONFIG_PATH);
                    $result['Config Directory Readable:'.$install_path] = new ResultInfo(is_readable($install_path), NULL, "<a href='installhelp.php#configpath'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");

                   
                    $result['Mod Rewrite Apache Configuration Setup Check'] = new ResultInfo(check_mod_rewrite(), NULL, "<a href='installhelp.php#rewrite'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    
                    $memcacheversion = "";
                    $isOK = memcacheserver_check($memcacheversion);
                    $result['PHP Memcache Server (optional)'] = new ResultInfo($isOK, NULL, "Version: ".$memcacheversion."<br/><a href='installhelp.php#memcache'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>Notes</a>");
                    
                }
                else
                {
                    // This checks only if the config files exist, if they were moved from default, then it might not be valid checks
                    $configpath = realpath(TONIDO_CLOUD_ROOT_DIR. DIRECTORY_SEPARATOR . 'config'. DIRECTORY_SEPARATOR.'cloudconfig.php');
                    $result['CloudConfig.php Readable:'.$configpath] = new ResultInfo(is_readable($configpath), NULL,  "Ignore failure if default config path has been changed");
                }
		return $result;
	}
        ?>
            
             <?php if (isset($_REQUEST['extended'])) 	
                           echo '<section class=\'extended-step\'>';
			else 
                           echo '<section class=\'checks\'>';

                  ?>	
             
                
                
		 <?php if (isset($_REQUEST['extended'])) 	
                           echo ' <h2><img src="images/check_shield_blue.svg" />Extended Checks</h2>';
			else 
                           echo '<h2><img src="images/check_shield_blue.svg" />Basic Checks</h2><h3>Checking FileCloud Requirements</h3>';

                  ?>					  
                 
                  <div class="section-content">
                      
                        <?php
                    if (isset($_REQUEST['extended']))
                    {
                        ?>
                    
                     <div class="step-number">
                      <?php echo $index; $index++; ?>
                    </div>
                    <div class="left-padding">
                        <div class="left-title">
                            <h3>Checking FileCloud Requirements</h3>
                        </div>
                    </div>
              <?php
                    }
                        ?>
                    
                      
                      <div class="checks-table left-padding no-margin">
				 
                       <table class="table table-hover">
                           <thead><tr>
                                   <th scope="col"><i class="fa fa-bars" aria-hidden="true"></i> Requirement</th>
                                   <th scope="col" class="text-center"><i class="fa fa-check-circle" aria-hidden="true"></i> Status</th>
                                   <th scope="col"><i class="fa fa-question-circle" aria-hidden="true"></i> Help</th></tr>
                           </thead>
                           <tbody>

                               <?php
                               $requirements = check_requirements();
                               $requirements_met = true;

                               foreach ($requirements as $name => $result):
                                   if (!$result->m_result)
                                       $requirements_met = false;
                                   ?>
                                   <tr><td <?php echo $result->m_result ? '' : 'class=\'error-label\' '  ?> ><?php echo $name ?></td><td class="table-status"><?php echo $result->m_result ? '<img src="images/ok.svg" />' : '<img src="images/error.svg" />' ?></td><td><?php echo isset($result->m_help)? $result->m_help :'' ?></td></tr>
                               <?php endforeach ?>
                           </tbody>
                        </table>
                    </div>
                </div>
                 
            </section>
                 
                 <?php
                    if (isset($_REQUEST['extended']))
                    {
                        
                        
                ?>
		<section class="extended-step grey-background">
                <div class="section-content">
                  <div class="step-number"><?php echo $index; $index++; ?></div>   
                  <div class="left-padding">
                         <div class="left-title">
                        <h3>Verification of Mongo DB connection</h3>
                </div>
                <?php
                    try{
                        $conn             = \core\framework\Utility::getMongoConnection(\core\framework\SettingsManager::getInstance()->getSetting("TONIDOCLOUD_DBSERVER"));
                        $siteDB           = \core\framework\Utility::getSiteSpecificDB("tonidoclouddb");
                        $db               = $conn->selectDB($siteDB);
                        //$adminDB          = $m->admin; //require admin priviledge
                        $mongodb_info     = $db->command(array('buildinfo'=>true));
                        $mongodb_version = '';
                        foreach ($mongodb_info as $info)
                        {
                            $mongodb_version = $info['version'];
                        }

                        if (strlen(trim($mongodb_version)) == 0)
                        {
                            echo '<p class=\'fail\'>MongoDB Version Empty: Maybe you are running a really old MongoDB version, check that you are using MongoDB > 2.0</b></p>';
                        }
                        else
                        {
                            echo '<p class="okay-text">MongoDB connection OK: MongoDB Version: <b>' . $mongodb_version. '</b></p>';
                            if (version_compare($mongodb_version, "3.2.0", "<"))
                            {
                                echo '<p class=\'fail\'>Warning: MongoDB Version is old, we recommend using MongoDB 3.2.x and above </b></p>';
                            }

                            if (version_compare(phpversion("mongodb"), "1.2.3", "<"))
                            {
                                echo '<p class=\'fail\'>Warning: PHP MongoDB Driver is too old, we recommend using PHP MongoDB ext driver 1.2.3 and above</b></p>';
                            }
                        }
                        echo '<p >We recommend you use MongoDB 3.2.x and above with PHP MongoDB ext driver 1.2.3 and above for best stability and performance.</p>';
                     }
                     catch (\core\framework\TonidoCloudException $e) {
                         echo '<p class=\'fail\'>MongoDB connection failed. MongoDB might not be installed or running.</p>';
                    } 
                    
                    

                ?>
                       </div>
                </div>
            </section>

		  <section class='extended-step'>
                <div class="section-content">

                    <div class="step-number"><?php echo $index; $index++; ?></div>
                    <div class="left-padding">
                        <div class="left-title">
                            <h3>FileCloud Update</h3>
                        </div>
                    </div>
                    
                   <div class="checks-table no-margin left-padding">
                <table class="table table-hover">
                    <thead>
                         <th scope="col"><i class="fa fa-puzzle-piece" aria-hidden="true"></i> Module</th>
                                                <th scope="col" class="text-center"><i class="fa fa-check-circle" aria-hidden="true"></i> Status</th>
                                                <th scope="col"><i class="fa fa-pencil" aria-hidden="true"></i> Notes</th>
                                                <th scope="col"><i class="fa fa-play-circle" aria-hidden="true"></i> Action</th></tr></thead>
                    </thead>
                    <tbody>
                        <?php
                        $upmgr = new admin\UpdateManager();

                        $dbok = $upmgr->IsDatabaseUptodate();
                        $configok = $upmgr->IsConfigUptodate();
                        $local = '0.0.0.0';
                        $remote = '0.0.0.0';
                        $infoArray = array();
                        $retcode = \core\framework\UpdateCheckManager::isBuildUptodate($local,$remote,$infoArray, 1);
                        
                        $dbstatus = ($dbok ? '<p class=\'success\'>OK</p>' : '<p class=\'FAIL\'>FAILED</p>');
                        $configstatus = ($configok ? '<p class=\'success\'>OK</p>' : '<p class=\'FAIL\'>FAILED</p>');

                        $dbNotes = "Current Database Version is " . $upmgr->getDatabaseVersion();
                        if ($dbok == false){
                            $dbNotes = "Your database version is " . $upmgr->getDatabaseVersion() . ".<br/> Required Version is " . $upmgr->getLatestDBVersion();
                        }

                        $configNotes = "All required constants are defined";
                        if ($configok == false){
                            $configNotes = "Required constants are missing";
                        }

                        $buildNotes = "Installed Version " . $local;
                        $buildstatus = "";
                        $buildAction = "";
                        if ($retcode === 1)
                        {
                            $buildstatus ='<p class=\'success\'>OK</p>';
                        }
                        if ($retcode === 2){
                            $buildNotes = "Installed Version " . $local . "<br/> Latest Version " . $remote;
                            $buildstatus ='<p class=\'success\'>UPDATE AVAILABLE</p>';
                            $buildAction = "<a href='https://www.getfilecloud.com/releasenotes/' class='btn btn-primary btn-sm' role='button'>Release Notes</a>";
                            
                            // add information on direct vs environment upgrade
                            if ($infoArray['allowdirectupgrade'])
                            {
                                $buildNotes .= "<br>Upgrade to the latest version from the Admin Portal";
                            }
                                
                            if ($infoArray['environmentupgradeneeded'])
                            {
                                $buildNotes .= "<br>Upgrade to the latest version by performing a full upgrade of your installation";
                            }
                            
                        }
                        else if ($retcode === 0){
                            $buildNotes = "Installed Version " . $local . "<br/> Latest Version is Unknown";
                        }

                        $dbAction = ($dbok ? "" : "<a href='update.php?mode=db' class=\"btn btn-primary btn-sm\">Update</a>");
                        $configAction = ($configok ? "" : "<a href='update.php?mode=config' class=\"button\">Update</a>");
                        ?>
                        <tr>
                            <td>Build Version</td>
                            <td class="table-status"><?php echo $buildstatus ?></td>
                            <td><?php echo $buildNotes ?></td>
                            <td><?php echo $buildAction ?></td>
                        </tr>
                        <tr>
                            <td>Database Schema</td>
                            <td class="table-status"><?php echo $dbstatus ?></td>
                            <td><?php echo $dbNotes ?></td>
                            <td><?php echo $dbAction ?></td>
                        </tr>
                        <tr>
                            <td>Config Files</td>
                            <td class="table-status"><?php echo $configstatus ?></td>
                            <td><?php echo $configNotes ?></td>
                            <td><?php echo $configAction ?></td>
                        </tr>

                    </tbody>
                </table>
               </div>
             </div>
            </section>

            
              <section class="extended-step grey-background">
                <div class="section-content">
                        <div class="step-number"><?php echo $index; $index++; ?></div>
                        <div class="left-padding">
                            <div class="left-title">
                                <h3>Access from outside</h3>
                            </div>
                            
		<p >To access FileCloud from outside your organization, you need to ensure that the
                Web server port <?php echo $_SERVER['SERVER_PORT']; ?> is accessible from outside. <br/> <br/>You can do this by
                port forwarding the ports from your Public WAN IP to the internal IP address of the FileCloud server. Ensure that you allow
                this port via any organizational firewalls. For additional security, it is recommended that you only use port 443 for secure access via HTTPS and you purchase and install a SSL certificate for your domain.
                </p>
                
                    </div>
                </div>
            </section>

                 <?php
                    }
                ?>
                
                <?php
                    if (!isset($_REQUEST['extended']))
                    {
                ?>
		
                    <section class="proceed-to-extended">
                        <div class="section-content">
                            <h2>Extended Checks</h2>
                            Basic checks OK? Proceed to <a class="button" href="index.php?extended=1">Extended Checks</a>
                        </div>
                    </section>
                    
                <?php
                    }
                ?>

                  <?php
                    if (isset($_REQUEST['extended'])) {
                        ?>
			
            <section class="extended-step">
                <div class="section-content">
                        <div class="step-number"><?php echo $index;
                            $index++; ?></div>
                        <div class="left-padding">
                            <div class="left-title">
                                <h3>Other Integrations</h3>
                            </div>
            
                     
                        <ul>
                            <li>Background Jobs: <B>IMPORTANT</B>: Setup the cron task service to ensure background tasks get executed. <a href="https://www.getfilecloud.com/supportdocs/display/cloud/Setting+up+Cron+Job+or+Scheduled+Task">More Info <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a> </li>
                            <li>Document Web Preview Support. <a href="https://www.getfilecloud.com/supportdocs/display/cloud/Setting+up+Document+Preview">More Info <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></li>
							<li>Active Directory or LDAP Authentication support (Optional) <a href="https://www.getfilecloud.com/supportdocs/display/cloud/Active+Directory+Authentication">More Info <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></li>
                            
                        </ul>
                          </div>
                </div>
            </section>
                        <?php
                    }
                  ?>

                 <?php
                    if (isset($_REQUEST['extended'])) {
                        ?>
            
             <section class="extended-step grey-background">
                <div class="section-content">
                        <div class="step-number"><?php echo $index; $index++; ?></div>
                        <div class="left-padding">
                            <div class="left-title">
                                <h3>Login</h3>
                            </div>
                            
			
                        <ul>
                            <?php $serverurl = $_SERVER['REQUEST_SCHEME'] . '://'.$_SERVER["HTTP_HOST"]; ?>
                            
                            <li>Login into the <b>Admin Portal</b> at <a href="<?php echo $serverurl.'/admin' ?>"> <?php echo $serverurl.'/admin' ?><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>. The default admin portal username is <b>"admin"</b> and admin password is <b>"password"</b>.</li>
                            <li>Login into the <b>User Portal</b> at <a href="<?php echo $serverurl.'/' ?>"> <?php echo $serverurl ?><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></li>
                        </ul>
                         <p>
                            If you face any issues or have any questions, please contact us at <b>support@codelathe.com</b>
                         </p>
                        
                           </div>
                </div>
            </section>
                        
                        <?php
                    }
                  ?>

                
                  <?php
                  if (isset($_REQUEST['extended'])) {
                        ?>
                 <section class="important">
                <div class="section-content">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    <h3>Important</h3>
                    <p>After successful installation, it is recommended to delete the <b>'install'</b> folder.</p>
                </div>
            </section>
                                        <?php
                  }
                  ?>
                
        </div>
             
            <footer>
                <div class="section-content">
                        FileCloud &copy; CodeLathe Technologies Inc 2008-2017
                </div>
            </footer>
            
    </body>
</html>