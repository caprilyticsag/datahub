<?php

/* * ***************************************************************************** 
  Copyright(c) 2012 CodeLathe LLC. All rights Reserved.
  This file is part of TonidoCloud  http://www.tonido.com
 * ***************************************************************************** */
define('TONIDO_CLOUD_ROOT_DIR', realpath(__DIR__ . '/..'));
require_once(TONIDO_CLOUD_ROOT_DIR . DIRECTORY_SEPARATOR . "admin". DIRECTORY_SEPARATOR ."updatemanager.php");
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>FileCloud Install</title>
    </head>
    <link rel="stylesheet" href="bootstrap.min.css">
    <style type="text/css"><!--
       html {
          position: relative;
          min-height: 100%;
        }
        body {
          /* Margin bottom by footer height */
          margin-bottom: 60px;
        }
        #footer {
          position: absolute;
          bottom: 0;
          width: 100%;
          /* Set the fixed height of the footer here */
          height: 60px;
          background-color: #f5f5f5;
        }
        body > .container {
          padding: 60px 15px 0;
        }
        .container .text-muted {
          margin: 20px 0;
        }

        #footer > .container {
          padding-right: 15px;
          padding-left: 15px;
        } 
       
        --></style>
    <body>
        <div class="container">
            <?php
                 if (!isset($_REQUEST['mode'])) {
                    echo 'Invalid Request';
                    exit(0);
                }

                $mode = $_REQUEST['mode'];

                $upmgr = new admin\UpdateManager();

                if ($mode == "db")
                {
                        $upmgr->updatedb();
                        echo '<h3>Database has been updated to Latest Version OK.</h3>';
                }
                if ($mode == "config")
                {
                    $upmgr->cloudconfigFileCheck();
                    echo "<br/><br/>";
                
                    if (!defined('TONIDOCLOUD_STORAGE_IMPLEMENTATION') || (defined('TONIDOCLOUD_STORAGE_IMPLEMENTATION') && (TONIDOCLOUD_STORAGE_IMPLEMENTATION == "local")))
                    {
                        $upmgr->localstorageConfigFileCheck();
                        echo "<br/><br/>";
                    }
                }
            ?>
            
            <A HREF="index.php?extended=1" class="btn btn-primary btn-sm" role="button">Go Back to Install</A>
        </div>
        
        
          <div id="footer">
                    <div class="container">
                        <p class="text-muted">Tonido FileCloud &copy; CodeLathe LLC</p>
                </div>
            </div>
    </body>
</html> 
