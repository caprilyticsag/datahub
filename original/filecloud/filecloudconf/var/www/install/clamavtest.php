<!DOCTYPE html>
<html>
 <head>
      <style type="text/css"><!--
      body {
        background-color: white;
        color: #039;
        font-size: 12px;
        font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
        }
        div#footer{font-size: 9px;}
        
        .success
        {
            color: #66ff66;
            padding: 0px;
            margin: 0px;
            font-weight: bold;
        }
        .fail
        {
            color: #ff3333;
            padding: 0px;
            margin: 0px;
            font-weight: bold;
        }
        .hor-minimalist-a
        {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 12px;
            background: #fff;
            margin: 45px;
            width: 1000px;
            border-collapse: collapse;
            text-align: left;
        }
        .hor-minimalist-a th
        {
            font-size: 14px;
            font-weight: normal;
            color: #039;
            padding: 10px 8px;
            border-bottom: 2px solid #6678b1;
        }
        .hor-minimalist-a td
        {
            color: #669;
            width: 200px;
            padding: 9px 8px 0px 8px;
        }
        .hor-minimalist-a tbody tr:hover td
        {
            color: #009;
        }
       
        --></style>
     
     
 </head>
        
<body>
<h2>FileCloud ClamAV Test</h2>
<?php

/* * ***************************************************************************** 
  Copyright(c) 2012 CodeLathe LLC. All rights Reserved.
  This file is part of TonidoCloud  http://www.tonido.com
 * ***************************************************************************** */

define('TONIDO_CLOUD_ROOT_DIR', realpath(__DIR__ . '/..'));
require_once(TONIDO_CLOUD_ROOT_DIR.DIRECTORY_SEPARATOR .'core'.DIRECTORY_SEPARATOR .'framework'.DIRECTORY_SEPARATOR .'runtime.php');

$clamav = new \core\framework\ClamAVCallback();
$clamav->test();


?>

<br/><br/>
<A HREF="index.php?extended=1">Click here to go back</A>
</body>
</html>