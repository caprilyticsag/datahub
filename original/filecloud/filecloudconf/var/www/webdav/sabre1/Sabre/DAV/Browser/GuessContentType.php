<?php

namespace Sabre\DAV\Browser;

use Sabre\DAV;

/**
 * GuessContentType plugin
 *
 * A lot of the built-in File objects just return application/octet-stream
 * as a content-type by default. This is a problem for some clients, because
 * they expect a correct contenttype.
 *
 * There's really no accurate, fast and portable way to determine the contenttype
 * so this extension does what the rest of the world does, and guesses it based
 * on the file extension.
 *
 * @copyright Copyright (C) 2007-2013 fruux GmbH (https://fruux.com/).
 * @author Evert Pot (http://evertpot.com/)
 * @license http://code.google.com/p/sabredav/wiki/License Modified BSD License
 */
class GuessContentType extends DAV\ServerPlugin {

    /**
     * List of recognized file extensions
     *
     * Feel free to add more
     *
     * @var array
     */
    public $extensionMap = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',
            'xml' => 'text/xml',
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',
            // audio/video
            'mp3' => 'audio/mpeg',
            'mp4' => 'video/mp4',
            'mkv' => 'video/mkv',
            'm4v' => 'video/m4v',
            'wmv' => 'video/x-ms-wmv',
            'mpeg' => 'video/mpeg',
            'mpg' => 'video/mpeg',
            'avi' => 'video/x-msvideo',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            'webm' => 'video/webm',
            'ogv' => 'video/ogg',
            'ogg' => 'application/ogg',
            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
            // ms office
            'doc' => 'application/msword',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'ppt' => 'application/vnd.ms-powerpoint',
            'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',

    );

    /**
     * Initializes the plugin
     *
     * @param DAV\Server $server
     * @return void
     */
    public function initialize(DAV\Server $server) {

        // Using a relatively low priority (200) to allow other extensions
        // to set the content-type first.
        $server->subscribeEvent('afterGetProperties',array($this,'afterGetProperties'),200);

    }

    /**
     * Handler for teh afterGetProperties event
     *
     * @param string $path
     * @param array $properties
     * @return void
     */
    public function afterGetProperties($path, &$properties) {

        if (array_key_exists('{DAV:}getcontenttype', $properties[404])) {

            list(, $fileName) = DAV\URLUtil::splitPath($path);
            $contentType = $this->getContentType($fileName);

            if ($contentType) {
                $properties[200]['{DAV:}getcontenttype'] = $contentType;
                unset($properties[404]['{DAV:}getcontenttype']);
            }

        }

    }

    /**
     * Simple method to return the contenttype
     *
     * @param string $fileName
     * @return string
     */
    protected function getContentType($fileName) {

        // Just grabbing the extension
        $extension = strtolower(substr($fileName,strrpos($fileName,'.')+1));
        if (isset($this->extensionMap[$extension]))
            return $this->extensionMap[$extension];

    }

}
