#!/bin/bash
if [ -f /var/www/app/serverlink/slcron.php ]
then
    CRONCMD="/var/www/app/serverlink/slcron.php"
elif [ -f /var/www/html/app/serverlink/slcron.php ]
then
    CRONCMD="/var/www/html/app/serverlink/slcron.php"
else
    echo "Serverlink cron not found"
    exit 1  
fi

for (( ; ; ))
do
   php -f $CRONCMD
   echo "ServerLink Client Exited... Restarting..."
   sleep 1
done
