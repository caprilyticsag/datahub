#!/bin/bash

CONFIGDIR=/var/www/config
LOCALSTORAGEKEY=TONIDO_LOCALSTORAGE_TOPLEVELFOLDER
LOCALSTORAGEFILE=$CONFIGDIR/localstorageconfig.php
BAKCONFIGFILE=$CONFIGDIR/backup-targets.list
MONGODBCONF=/etc/mongodb.conf
DIRNAME=`dirname $0`
DIRNAME=`readlink -f $DIRNAME`
APACHEUSER="www-data"
APACHEGRP="www-data"
BAKPREFIX=tonidobak
LOGFILENAME=$BAKPREFIX.log
DATFILENAME=$BAKPREFIX.dat
DBBAKFILENAME="${BAKPREFIX}_db.tgz"
FILESINPROGRESSID=$BAKPREFIX.filesinprogress
FILESINCOMPLETEID=$BAKPREFIX.filesincomplete
FILESCOMPLETEID=$BAKPREFIX.filescomplete
DBINPROGRESSID=$BAKPREFIX.dbinprogress

MONGODBSVC=/etc/init.d/mongodb
RESTORELOGFILE="/tmp/${BAKPREFIX}_restore.log"

PRIVATEKEY=$DIRNAME/keys/id_rsa
#PRIVATEKEY=/tmp/rad/id_rsa
PUBLICKEY="${PRIVATEKEY}.pub"
LOCALHOST="127.0.0.1"
SSHCMD="ssh -o PasswordAuthentication=no -i $PRIVATEKEY"
declare BAKUPDIRS

######UTIL FUNCTIONS#########

# Log message
echoLog()
{
        TIMESTAMP=`date +"%b %d %T : "`
        #echo "$TIMESTAMP""${1}"  >> $DEBUGLOG
        echo "$TIMESTAMP""${1}" 
}

restoreLog()
{
       echo "${1}"  >> $RESTORELOGFILE 
}
trim()
{
	echo "${1}" 
}

checkInput(){
    KEY=$1
    VALUE=$2
    if [[ ! -z ${VALUE} ]]; then
        return 0;
    fi
    case $KEY in
        USER)
            echoLog "Missing user name"
            exit $MISSING_HOST_USER
            ;;              
        IP)
            echoLog "Missing machine IP"
            exit $MISSING_HOST_USER
            ;;              
        TGTDIR)
            echoLog "Missing target directory"
            exit $MISSING_TGT_DIR
            ;;              
    esac

}

normalizePath()
{
    DIRPATH=`dirname "${1}" `
    if [[ $DIRPATH == "/" ]]; then
        DIRPATH=""
    fi
    DIRNAME=`basename "${1}" `
    echo $DIRPATH"/"$DIRNAME
}

latestDir()
{
    find ${1} -mindepth 1 -maxdepth 1 -type d  | sort | tail -1
}

getTopBackupDir()
{
    TOPBAKDIR=`php $DIRNAME/backuphelper.php gettopstoragedir`
    if [[ ! -z $TOPBAKDIR ]]; then
        echo $TOPBAKDIR
    else
        echoLog "Invalid cloud storage directory $TOPBAKDIR"
        exit $BACKUP_SRC_NOT_EXISTS
    fi
}

######BACKUP INIT FUNCTIONS#########

# check if system has been initialized
initialize(){
    initialize1
    if [[ -f $PRIVATEKEY ]]; then
        return 0
    else
        echoLog "Initializing backup system"
        initialize2
        
        USER=
        while [[ -z $USER ]]; do
            echoLog "Please enter local user to store backups :"
            read USER
        done
        addHost $USER $LOCALHOST
        if [ $? -ne 0 ]; then
            echoLog "Unable to add login to localhost"
            exit $LOCALHOST_LOGIN_ERR
        else
            echoLog "Successfully initialized localhost to store backups"
            return $SUCCESS;
        fi           
    fi
}

#load error codes
initialize1(){
    # error codes
    SUCCESS=0
    BACKUP_TGT_OPN_SUCCESS=0
    BACKUP_TGT_NOT_VALID=11
    BACKUP_TGT_EXISTS_ALREADY=12
    BACKUP_TGT_NOT_EXISTS=13
    BACKUP_SRC_NOT_EXISTS=14
    BACKUP_DIR_NOT_VALID=15
    BACKUP_TGT_LIST_EMPTY=16
    BACKUP_DIR_PERM_DENIED=17
    MISSING_HOST_IP=21
    MISSING_HOST_USER=22
    MISSING_TGT_DIR=23
    MISSING_LOCAL_STG_CONFIG=24
    SSH_KEY_GEN_ERR=31
    LOCALHOST_LOGIN_ERR=32
}

#generate private keys
initialize2(){
    echoLog "Generating ssh encryption keys"
    KEYDIR=`dirname $PRIVATEKEY`
    mkdir -p $KEYDIR
    ssh-keygen -t rsa -N "" -f $PRIVATEKEY
    if [ $? -ne 0 ]; then
        echoLog "Error generating ssh encryption keys"
        exit $SSH_KEY_GEN_ERR
    fi
}

######BACKUP MGMT FUNCTIONS#########
###################################
# Create backup target
###################################
addBackupTgt()
{
    USER=$1
    IP=$2
    TGTDIR=$3
    
    checkInput USER $USER
    checkInput IP $IP
    checkInput TGTDIR $TGTDIR

    echoLog "Adding backup target : ${USER}@${IP}:${TGTDIR}" 

    addHost $USER $IP

    DIRCHKCMD="file ${TGTDIR} > /dev/null 2>&1"
    $SSHCMD ${USER}@${IP} $DIRCHKCMD
    if [ $? -ne 0 ]; then
        echoLog "${TGTDIR} not found at ${USER}@${IP}"
        exit $BACKUP_TGT_NOT_EXISTS
    fi  

    PERMCHKCMD="touch ${TGTDIR}/.test > /dev/null 2>&1 && rm -f ${TGTDIR}/.test > /dev/null 2>&1"
    $SSHCMD ${USER}@${IP} $PERMCHKCMD
    if [ $? -ne 0 ]; then
        echoLog "No write permissions for ${TGTDIR} at ${USER}@${IP}"
        exit $BACKUP_DIR_PERM_DENIED
    fi  
		
    TGTDIR=`normalizePath "${TGTDIR}"`
    NEWENTRY="${USER}@${IP}:${TGTDIR}"
    FOUND=`sed -n "\|^$NEWENTRY$|p" $BAKCONFIGFILE`
    if [[ -z $FOUND ]]; then
        echo $NEWENTRY >> $BAKCONFIGFILE
        echoLog "Added backup target : ${NEWENTRY}" 
        exit $SUCCESS
    else	
        echoLog "Backup target already exists : ${NEWENTRY}" 
        exit $BACKUP_TGT_EXISTS_ALREADY
    fi			
}

#add host
addHost(){
    USER=$1
    IP=$2

    checkInput USER $USER
    checkInput IP $IP
    
    RESULT=`testConnectivity $USER $IP`

    if [ $? -eq 0 ]; then
        return 0
    else
        echoLog "Exchanging ssh keys with host $IP"
        cat $PUBLICKEY | ssh $USER@$IP "mkdir -p ~/.ssh/ && cat - >> ~/.ssh/authorized_keys"
        return $?
    fi
}

###################################
# Remove backup target
###################################
removeBackupTgt()
{
    TGTINDEX=${1}

    if [[ -z $TGTINDEX ]]; then
        listBackupTgts
        echoLog "Select a backup target to remove from the above list"
        read TGTINDEX
    else
        loadBackupTgts
    fi
    
    if [ $TGTINDEX -lt ${#BAKTGTS[@]} ]; then
        echoLog "Selected backup target ${BAKTGTS[$TGTINDEX]}"        
        DELENTRY=${BAKTGTS[$TGTINDEX]}
        sed -i -e "\|^$DELENTRY$|d" $BAKCONFIGFILE
        echoLog "Backup target removed : ${DELENTRY}" 
    else
        echoLog "Invalid target index specified"
        exit $BACKUP_TGT_NOT_VALID
    fi	
}

###################################
# List backup targets
###################################
listBackupTgts()
{
    loadBackupTgts
    INDEX=0        
    echo "  No         Targets"
    echo "-------------------------------------------------------------"
    while [ $INDEX -lt ${#BAKTGTS[@]} ]; do
        echo "  $INDEX)         ${BAKTGTS[$INDEX]}"
        let INDEX=$INDEX+1
    done
    echo "-------------------------------------------------------------"

    if [ $INDEX -eq 0 ]; then
        echo "No backup targets found"
        echo "Note: Please add backup targets with addtgt option"
        exit $BACKUP_TGT_LIST_EMPTY
    fi
}

loadBackupTgts()
{
    BAKTGTS=( `cat ${BAKCONFIGFILE}` )
}

###################################
# Create backup functions
###################################
createBackup()
{
    SRCDIR=`getTopBackupDir`
    TGTINDEX=${1}
    
    echoLog "Creating backup"
    if [[ -z $TGTINDEX ]]; then
        listBackupTgts
        echo -n "Select a backup target from the above list : "
        read TGTINDEX
    else
        loadBackupTgts
    fi

    if [ $TGTINDEX -lt ${#BAKTGTS[@]} ]; then
        echoLog "Selected backup target ${BAKTGTS[$TGTINDEX]}"        
        BAKTGT=${BAKTGTS[$TGTINDEX]}
        USER=`echo $BAKTGT |  cut -f 1 -d '@' `
        IP=`echo $BAKTGT |  cut -f 2 -d '@' | cut -f 1 -d :`
        TGTDIR=`echo $BAKTGT |  cut -f 2 -d '@' | cut -f 2 -d :`
        backupFiles ${SRCDIR} ${USER} ${IP} ${TGTDIR}
    else
        echoLog "Invalid target index specified"
        exit $BACKUP_TGT_NOT_VALID
    fi
}


#####
# $1 = srcdir
# $2 = user
# $3 = ip
# $4 = tgtdir
#####
backupFiles()
{
    SRCDIR=${1}
    USER=${2}
    IP=${3}    
    TGTDIR=${4}
   

    TIMESTAMP=`date '+%s'`
    TOPBAKDIR="$TGTDIR/$TIMESTAMP"
    LASTDIRCMD="find $TGTDIR -mindepth 1 -maxdepth 1 -type d  | sort | tail -1"
    LASTBAKDIR=`ssh -i $DIRNAME/keys/id_rsa ${USER}@${IP} $LASTDIRCMD`
    FILEBAKDIR="$TOPBAKDIR/files"
    DBBAKDIR="$TOPBAKDIR/db"
    BAKLOGFILE="$TOPBAKDIR/$LOGFILENAME"
    BAKDATFILE="$TOPBAKDIR/$DATFILENAME"
    
    #create dir structure	
    STARTCMD="mkdir -p $FILEBAKDIR; mkdir -p $DBBAKDIR; touch $TOPBAKDIR/$FILESINPROGRESSID"
    $SSHCMD ${USER}@${IP} $STARTCMD

    #backup
    if [[ -z $LASTBAKDIR ]]; then
    	rsync -a -h -e "$SSHCMD" -P $SRCDIR/* $USER@$IP:$FILEBAKDIR/ --log-file=/tmp/$LOGFILENAME
        scp -i $DIRNAME/keys/id_rsa /tmp/$LOGFILENAME ${USER}@${IP}:$BAKLOGFILE
    else
    	rsync -a -h -e "$SSHCMD" --delete -P --link-dest=$LASTBAKDIR/files/ $SRCDIR/* $USER@$IP:$FILEBAKDIR/ --log-file=/tmp/$LOGFILENAME
        scp -i $DIRNAME/keys/id_rsa /tmp/$LOGFILENAME ${USER}@${IP}:$BAKLOGFILE
    fi
	
    #check data consistency
    COUNT=`rsync -avn  -e "ssh -i $DIRNAME/keys/id_rsa" $SRCDIR/* $USER@$IP:$FILEBAKDIR/ | grep dat `
    FINCMD="rm -f $TOPBAKDIR/$FILESINPROGRESSID"
    if [[ ! -z $COUNT ]]; then
    	FINCMD="$FINCMD; touch $TOPBAKDIR/$FILESINCOMPLETEID"
    else
    	FINCMD="$FINCMD; touch $TOPBAKDIR/$FILESCOMPLETEID"
    fi
    

    #add backup stats
    FINCMD="$FINCMD; echo '`date -d \@$TIMESTAMP "+%T %Y-%m-%d"`' >> $BAKDATFILE"
    FINCMD="$FINCMD; echo -n | >> $BAKDATFILE"
    FINCMD="$FINCMD; du -hs $FILEBAKDIR | cut -f 1 >> $BAKDATFILE"
    FINCMD="$FINCMD; echo -n | >> $BAKDATFILE"
    FINCMD="$FINCMD; find $FILEBAKDIR -name '*.dat' | wc -l >> $BAKDATFILE"

    #execute backup finish script
    ssh -i $DIRNAME/keys/id_rsa ${USER}@${IP} $FINCMD
	
    backupDB $USER $IP $TOPBAKDIR
}

backupDB()
{
	USER=${1}
	IP=${2}
	TOPBAKDIR=${3}
	DBBAKFILE="$TOPBAKDIR/db/$DBBAKFILENAME"
	
	STARTCMD="touch $TOPBAKDIR/$DBINPROGRESSID"
	ssh -i $DIRNAME/keys/id_rsa ${USER}@${IP} $STARTCMD
	
	SRCDBDIR=`cat /etc/mongodb.conf | grep dbpath | cut -f 2 -d =`
	if [[ ! -d $SRCDBDIR ]]; then
		echoLog "Invalid DB directory"
		exit 1
	fi
	cd $SRCDBDIR
	tar zcf - * | ssh -i $DIRNAME/keys/id_rsa $USER@$IP "cat - > $DBBAKFILE"
	FINCMD="rm -f $TOPBAKDIR/$DBINPROGRESSID"
	ssh -i $DIRNAME/keys/id_rsa ${USER}@${IP} $FINCMD
}



###################################
# List backup directories
###################################
listBackups()
{
    TGTINDEX=${1}
    if [[ -z $TGTINDEX ]]; then
        listBackupTgts
        echoLog "Select a target from above to list available backups"
        read TGTINDEX
    else
        loadBackupTgts
    fi
    
    if [ $TGTINDEX -lt ${#BAKTGTS[@]} ]; then
        echoLog "Selected backup target ${BAKTGTS[$TGTINDEX]}"        
        BAKTGT=${BAKTGTS[$TGTINDEX]}
        USER=`echo $BAKTGT |  cut -f 1 -d '@' `
        IP=`echo $BAKTGT |  cut -f 2 -d '@' | cut -f 1 -d :`
        TGTDIR=`echo $BAKTGT |  cut -f 2 -d '@' | cut -f 2 -d :`
        listBackupsTable ${USER} ${IP} ${TGTDIR}
    else
        echoLog "Invalid target index specified"
        exit $BACKUP_TGT_NOT_VALID
    fi
}

listBackupsTable()
{
    loadBackups $1 $2 $3
    echo "  No         Date                         Files         Size              Status              Path"
    echo "---------------------------------------------------------------------------------------------------------------"
    
    INDEX=0
    while [ $INDEX -lt ${#BAKUPDIRS[@]} ]; do
        BAKUPDIR=${BAKUPDIRS[$INDEX]}
        TIMESTAMP=`echo $BAKUPDIR | cut -f 1 -d '|'`
        NOFILES=`echo $BAKUPDIR | cut -f 2 -d '|'`
        SIZE=`echo $BAKUPDIR | cut -f 3 -d '|'`
        STATUS=`echo $BAKUPDIR | cut -f 4 -d '|'`
        BAKPATH=`echo $BAKUPDIR | cut -f 5 -d '|'`
        echo "  $INDEX)         $TIMESTAMP          $NOFILES          $SIZE               $STATUS            $BAKPATH"
        let INDEX=$INDEX+1
    done
    echo "---------------------------------------------------------------------------------------------------------------"
}

listBackupsXML()
{
    USER=${1}
    IP=${2}
    BAKTGTDIR=${3}
    
	
	
    LISTCMD="ls -1 $BAKTGTDIR | sort -r"
    XMLRESULT="<backups>"
    for f in `ssh -i $DIRNAME/keys/id_rsa ${USER}@${IP} $LISTCMD`
    do
	TOPBAKDIR="$BAKTGTDIR/$f"
	BAKDATFILE="$TOPBAKDIR/$DATFILENAME"
	
	XMLRESULT="$XMLRESULT <backup>"
	XMLRESULT="$XMLRESULT <path>$TOPBAKDIR</path>"
	
	STATCHKCMD="ls -1 $TOPBAKDIR/$BAKPREFIX.*";
	STATRESULTS=`ssh -i $DIRNAME/keys/id_rsa ${USER}@${IP} $STATCHKCMD`
	DBPROG=`echo $STATRESULTS | grep $DBINPROGRESSID`
	FLPROG=`echo $STATRESULTS | grep $FILESINPROGRESSID`
	FLINCO=`echo $STATRESULTS | grep $FILESINCOMPLETEID`
	FLCOMP=`echo $STATRESULTS | grep $FILESCOMPLETEID`
	if [[ ! -z $DBPROG ]]; then
            XMLRESULT="$XMLRESULT <status>dbinprogress</status>"
        elif [[ ! -z $FLINCO ]]; then
            XMLRESULT="$XMLRESULT <status>incomplete</status>"
        elif [[ ! -z $FLCOMP ]]; then
            XMLRESULT="$XMLRESULT <status>complete</status>"
        elif [[ ! -z $FLPROG ]]; then
            XMLRESULT="$XMLRESULT <status>inprogress</status>"
	fi			
	DATCHKCMD="cat $TOPBAKDIR/$DATFILENAME 2> /dev/null";
	DATRESULTS=`ssh -i $DIRNAME/keys/id_rsa ${USER}@${IP} $DATCHKCMD`
	XMLRESULT="$XMLRESULT $DATRESULTS"
	XMLRESULT="$XMLRESULT </backup>"
    done
    XMLRESULT="$XMLRESULT </backups>"
    echo $XMLRESULT
}


loadBackups()
{
    USER=${1}
    IP=${2}
    BAKTGTDIR=${3}
    
    INDEX=0
    LISTCMD="ls -1 $BAKTGTDIR | sort -r"
    for f in `$SSHCMD ${USER}@${IP} $LISTCMD`
    do
        TOPBAKDIR="$BAKTGTDIR/$f"
	BAKDATFILE="$TOPBAKDIR/$DATFILENAME"
        DATCHKCMD="cat $TOPBAKDIR/$DATFILENAME | tr '\n' '|' 2> /dev/null";
	DATRESULTS=`$SSHCMD ${USER}@${IP} $DATCHKCMD`
        STATUS="UNKNOWN"
	STATCHKCMD="ls -1 $TOPBAKDIR/$BAKPREFIX.*";
	STATRESULTS=`ssh -i $DIRNAME/keys/id_rsa ${USER}@${IP} "${STATCHKCMD}"`
	DBPROG=`echo $STATRESULTS | grep $DBINPROGRESSID`
	FLPROG=`echo $STATRESULTS | grep $FILESINPROGRESSID`
	FLINCO=`echo $STATRESULTS | grep $FILESINCOMPLETEID`
	FLCOMP=`echo $STATRESULTS | grep $FILESCOMPLETEID`
	if [[ ! -z $DBPROG ]]; then
            STATUS="DBINPROGRESS"
        elif [[ ! -z $FLINCO ]]; then
            STATUS="INCOMPLETE"
        elif [[ ! -z $FLCOMP ]]; then
            STATUS="COMPLETE"
        elif [[ ! -z $FLPROG ]]; then
            STATUS="FILESINPROGRESS"
	fi
        BAKUPDIRS[$INDEX]="$DATRESULTS$STATUS|$TOPBAKDIR"
        let INDEX=$INDEX+1
    done
}

###################################
# Remove backup function
###################################

removeBackup()
{
    TGTINDEX=${1}
    if [[ -z $TGTINDEX ]]; then
        listBackupTgts
        echoLog "Select a target from above to list available backups"
        read TGTINDEX
    else
        loadBackupTgts
    fi
    
    if [ $TGTINDEX -lt ${#BAKTGTS[@]} ]; then
        BAKINDEX=${2}
        echoLog "Selected backup target ${BAKTGTS[$TGTINDEX]}"        
        BAKTGT=${BAKTGTS[$TGTINDEX]}
        USER=`echo $BAKTGT |  cut -f 1 -d '@' `
        IP=`echo $BAKTGT |  cut -f 2 -d '@' | cut -f 1 -d :`
        TGTDIR=`echo $BAKTGT |  cut -f 2 -d '@' | cut -f 2 -d :`
       
        if [[ -z $BAKINDEX ]]; then
            listBackupsTable ${USER} ${IP} ${TGTDIR}
            echoLog "Select a backup directory"
            read BAKINDEX
        else
            loadBackups ${USER} ${IP} ${TGTDIR}
        fi
        
        if [ $BAKINDEX -lt ${#BAKUPDIRS[@]} ]; then
            BAKUPDIR=${BAKUPDIRS[$BAKINDEX]}
            BAKPATH=`echo $BAKUPDIR | cut -f 5 -d '|'`
            echoLog "Selected backup path ${BAKPATH}"        
            RMCMD="rm -rf $BAKPATH"
            $SSHCMD ${USER}@${IP} $RMCMD
            exit $?
        else
            echoLog "Invalid backup directory specified"
            exit $BACKUP_DIR_NOT_VALID
        fi
       
    else
        echoLog "Invalid target index specified"
        exit $BACKUP_TGT_NOT_VALID
    fi

}


###################################
# Restore functions               
###################################

restoreBackup()
{
    TGTINDEX=${1}
    if [[ -z $TGTINDEX ]]; then
        listBackupTgts
        echoLog "Select a target from above to list available backups"
        read TGTINDEX
    else
        loadBackupTgts
    fi
    
    if [ $TGTINDEX -lt ${#BAKTGTS[@]} ]; then
        BAKINDEX=${2}
        echoLog "Selected backup target ${BAKTGTS[$TGTINDEX]}"        
        BAKTGT=${BAKTGTS[$TGTINDEX]}
        USER=`echo $BAKTGT |  cut -f 1 -d '@' `
        IP=`echo $BAKTGT |  cut -f 2 -d '@' | cut -f 1 -d :`
        TGTDIR=`echo $BAKTGT |  cut -f 2 -d '@' | cut -f 2 -d :`
       
        if [[ -z $BAKINDEX ]]; then
            listBackupsTable ${USER} ${IP} ${TGTDIR}
            echoLog "Select a backup directory"
            read BAKINDEX
        else
            loadBackups ${USER} ${IP} ${TGTDIR}
        fi
        
        if [ $BAKINDEX -lt ${#BAKUPDIRS[@]} ]; then
            BAKUPDIR=${BAKUPDIRS[$BAKINDEX]}
            BAKPATH=`echo $BAKUPDIR | cut -f 5 -d '|'`
            echoLog "Selected backup path ${BAKPATH}"        
            TGTDIR=`getTopBackupDir`
            restoreFiles $USER $IP $BAKPATH $TGTDIR
            exit $?
        else
            echoLog "Invalid backup directory specified"
            exit $BACKUP_DIR_NOT_VALID
        fi
       
    else
        echoLog "Invalid target index specified"
        exit $BACKUP_TGT_NOT_VALID
    fi
}

#####
# $1 = user 
# $2 = ip
# $3 = srcdir - backup directory containing all files
# $4 = tgtdir - cloud storage directory
#####
restoreFiles()
{
    USER=$1
    IP=$2
    TOPBAKDIR=${3}
    SRCDIR=$TOPBAKDIR/files
    TGTDIR=${4}
    
    rm -rf $TGTDIR/*
    rsync -a -h -e "ssh -i $DIRNAME/keys/id_rsa" -P $USER@$IP:$SRCDIR/* $TGTDIR/ --log-file=$RESTORELOGFILE > /dev/null 2>&1;
    chown -R $APACHEUSER.$APACHEGRP $TGTDIR/
    restoreLog "Restore files completed." 
    restoreDB $USER $IP $TOPBAKDIR
}

restoreDB()
{
        restoreLog "Restore database." 
	USER=${1}
	IP=${2}
	TOPBAKDIR=${3}
	DBBAKFILE="$TOPBAKDIR/db/$DBBAKFILENAME"
        
	TGTDBDIR=`cat /etc/mongodb.conf | grep dbpath | cut -f 2 -d =`
	if [[ ! -d $TGTDBDIR ]]; then
		echoLog "Invalid DB directory"
		exit 1
	fi
        restoreLog "Stopping mongodb."
        $MONGODBSVC stop
	cd $TGTDBDIR
        restoreLog "Removing existing database ."
        rm -rf *
        restoreLog "Restoring database from backup."
        ssh -i $DIRNAME/keys/id_rsa $USER@$IP "cat $DBBAKFILE" | tar zxf -
        rm -f $TGTDBDIR/mongod.lock
        restoreLog "Starting mongodb."
        $MONGODBSVC start
        restoreLog "Restore database completed." 
}


###################################
# Connection test function
###################################
# $1 = user
# $2 = ip
testConnectivity()
{
    # ssh -i $PRIVATEKEY ${1}@${2} "date"
    $SSHCMD ${1}@${2} "date" 2> /dev/null
    return $?
}

###################################
# Usage
###################################
printUsage()
{
    echo "Script to backup tonidocloud data"
    echo "Usage :"
    echo "  sudo /path/backup.sh <command>"
    echo "  where <command> could be on of the following:"
    echo "          addtgt <user> <host> <tgtdir> - Adds the <tgtdir> in machine <user>@<host> as a valid backup directory"
    echo "          rmtgt <index>                 - Removes the backup target at specified index." 
    echo "                                          If index parameter is missing, a list of available targets will be shown to choose one from."
    echo "          lstgts                        - Lists available backup directories"
    echo "          crtbkup <index>               - Create a new backup of tonidocloud at specified backup target index. "
    echo "                                          If index parameter is missing, a list of available targets will be shown to choose one from."
    echo "          lstbkups <index>              - List all backups under the specified target index."
    echo "                                          If index parameter is missing, a list of available targets will be shown to choose one from."
    echo "          rmbkup <index1> <index2>      - Removes index2 directory from index1 backup target. "
    echo "                                          If index parameters is missing, a lists of available targets and directories will be shown to choose one from."
    echo "          resbkup <index1> <index2>     - Restores index2 directory from index1 backup target. "
    echo "                                          If index parameters is missing, a lists of available targets and directories will be shown to choose one from."
}

USER=`whoami`
if [[ "$USER" != "root" ]]; then
    echo "Please run the script as root user or with sudo access"
    exit 1;
fi

initialize
RC=
case $1 in
    addtgt)
        shift
        addBackupTgt ${1} ${2} ${3}
        RC=$?
        ;;
    rmtgt)
        shift
        removeBackupTgt ${1}
        RC=$?
        ;;
    lstgts)
        shift
        listBackupTgts
        RC=$?
        ;;
    crtbkup)
        shift
        createBackup ${1}
        RC=$?
        ;;
    rmbkup)
        shift
        removeBackup ${1} ${2}
        RC=$?
        ;;                 
    lstbkups)
        shift
        listBackups ${1}
        RC=$?
        ;;
    resbkup)
        shift
        restoreBackup ${1} ${2}
        RC=$?
        ;;            

    backup)
        shift
        backupFiles ${1} ${2} ${3} ${4}
        RC=$?
        ;;               
    listbackups)
        shift
        listBackupsXML ${1} ${2} ${3}
        RC=$?
        ;;                    
    restore)
        shift
        restoreFiles ${1} ${2} ${3} ${4}
        RC=$?
        ;;            
    test)
        shift
        testConnectivity ${1} ${2}
        RC=$?
        ;;        
    *)
        printUsage
        ;;
esac

exit $RC
