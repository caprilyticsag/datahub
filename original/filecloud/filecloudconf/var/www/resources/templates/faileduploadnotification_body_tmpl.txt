<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>FileCloud Change Notification</title>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
<!-- 
NOTE:
- There's a long line of spaces above this note. That's because iOS email clients don't download POP emails if the head is shorter than 1,019 characters
- All images have style="display:block". This forces Gmail to display the table cells correctly
- style="font-size:1px" is needed in empty table cells smaller than 15 pixels. Outlook 2013 adds a 15px whitespace by default to these cells, unless there is a font-size set to 1px
-->
	
</head>
<body style="margin:0px; padding:0px; background:#ffffff">
    <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="margin: auto">
        <tr>
            <td colspan="3"><img src="^CLOUDURL^/core/getcustomimage?type=login" alt="fileCloud" style="display:block"></td>
        </tr>
        <tr>
            <td style="background: #1265a0;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="background: #1265a0; color: #ffffff"><br>
                <h2 style="font-family: Arial, Helvetica; font-size: 25px; font-weight: normal; padding: 0px; margin: 0px">Storage Quota Exceeded</h2>
                <p style="font-family: Arial, Helvetica; font-size: 14px; padding: 0px; margin: 0px"></p>
                <br>
            </td>
            <td style="background: #1265a0;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>        
        </tr>
        <tr>
            <td style="background: #f7f7f7;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>    
            <td style="background: #f7f7f7; font-family: Arial, Helvetica; font-size: 14px; line-height: 20px; text-align:left">
				<br/>
                <p>Your ^SERVICENAME^ account is running out of the allocated ^TOTALSIZE^ disk space. Please login to ^SERVICENAME^ and delete unused files or contact your administrator.</p>			
                <br/>
            </td>
            <td style="background: #f7f7f7;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>            
        </tr>
        <tr>
            <td colspan="3" style="text-align:center"><br>
                <table cellpadding="0" cellspacing="0" border="0" align="center" style="margin:auto">
				    <tr><td style="font-family: Arial, Helvetica; font-size: 10px; color: #909090">You received this message because you signed up for an account at FileCloud.</td></tr>
					<tr><td style="font-family: Arial, Helvetica; font-size: 10px; color: #909090">Unsubscribe: ^CLOUDURL^/core/unsubscribe?emailid=^EMAIL^</td></tr>
                    <tr><td style="font-family: Arial, Helvetica; font-size: 10px; color: #909090">Powered by <a style="font-family: Arial, Helvetica; font-size: 10px; color: #909090" href="http://www.getfilecloud.com"> FileCloud</a></td></tr>
                </table>
                <br>
            </td>
        </tr>
    </table>
</body>
</html>