<?php

defined('TONIDO_CLOUD_ROOT_DIR') or exit('Forbidden');

/* Usually kept away from WWW available paths */
/* You can move these locations outside the WWW document root for better security */

define("TONIDOCLOUD_SCRATCH_PATH", TONIDO_CLOUD_ROOT_DIR.DIRECTORY_SEPARATOR."scratch"); // < VALID scratch directory path, needs to be writable by www-data
define("TONIDOCLOUD_CONFIG_PATH", TONIDO_CLOUD_ROOT_DIR.DIRECTORY_SEPARATOR."config"); // < VALID config directory path, needs to be readable by www-data

require_once(TONIDOCLOUD_CONFIG_PATH.DIRECTORY_SEPARATOR.'cloudconfig.php'); // Valid configuration directory path, needs to be readable by www-data

?>