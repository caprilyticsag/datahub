FROM ubuntu:16.04 

LABEL com.example.version="0.0.1-beta"
LABEL vendor="Caprilytics AG"
LABEL com.example.release-date="2018-10-15"
LABEL com.example.version.is-production=""

######### Base installation for Ubuntu ###############
### Prepare the system ############################
RUN apt-get update && apt-get install -y \ 
    language-pack-en-base \
    software-properties-common \
    apt-utils \
    bash

### Install latest Oracle Java
RUN add-apt-repository "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main"
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
RUN echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections
RUN apt-get update && apt-get install -y \ 
    oracle-java8-installer

######### Install Webserver ###################################

### Installing apache #############
RUN apt-get update && apt-get install -y \
    unzip \
    apache2 \
    build-essential \
    libsslcommon2-dev \
    libssl-dev \
    pkg-config \
    memcached \
    rsync \
    curl

### Adding PHP repository ###############
RUN LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php
RUN apt-get update && apt-get install -y \
    php7.1 \
    php7.1-cli \
    php7.1-common \
    php-pear \
    php-dev

### Install PHP extensions required by Laravel
RUN apt-get update && apt-get install -y \
    php7.1-json \
    php7.1-opcache \
    php7.1-mbstring \
    php7.1-mcrypt \
    php7.1-zip \
    php7.1-memcache \
    php7.1-xml \
    php7.1-bcmath \
    libapache2-mod-php7.1 \
    php7.1-gd \
    php7.1-curl \
    php7.1-ldap \
    php7.1-gmp \
    php7.1-intl

### Install MongoDB driver for PHP 7.1 ##############
RUN apt-get update -y && apt-get install -y \
    php-mongodb

### Set PHP environment ##############
RUN a2dismod php7.1
RUN a2enmod php7.1
RUN service apache2 restart
RUN update-alternatives --set php /usr/bin/php7.1

### Installing IONCUBE PHP driver ##
COPY ioncube/* /usr/lib/php/7.1/
RUN echo "zend_extension = '/usr/lib/php/7.1/ioncube/ioncube_loader_lin_7.1.so'" > /etc/php/7.1/mods-available/01-ioncube.ini
RUN ln -sfn /etc/php/7.1/mods-available/01-ioncube.ini /etc/php/7.1/apache2/conf.d/
RUN ln -sfn /etc/php/7.1/mods-available/01-ioncube.ini /etc/php/7.1/cli/conf.d/
RUN service apache2 restart 

######### Installing the MongoDB

### Import the Public Key used by the Ubuntu Package Manager
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6 

### Create a file list for mongoDB to fetch the current repository
RUN echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.4.list

### Update the Ubuntu Packages
RUN apt update -y

### Install MongoDB
RUN apt-get update && apt-get install -y \ 
    mongodb-org

### The hack underneath is done to make sure, that service mongodb start works! It is not pretty, but the only solution
RUN apt-get update && apt-get install -y \  
    mongodb
RUN apt-get update && apt-get install -y \ 
    mongodb-org
RUN systemctl enable mongod
RUN service mongodb start
RUN service apache2 start

######### Installing FileCloud
COPY filecloud/*.deb /tmp/
WORKDIR /tmp
RUN dpkg -i ./filecloudprereq.deb
RUN dpkg -i ./filecloudconf.deb


########## Installing LibreOffice
RUN add-apt-repository ppa:libreoffice/ppa
RUN apt-get update && apt-get install -y \
    libreoffice
RUN if [ ! -L /usr/bin/libreoffice ]; then ln -s /usr/bin/libreoffice6.0 /usr/bin/libreoffice ; fi
RUN if [ ! -L /usr/lib/libreoffice ]; then ln -s /opt/libreoffice6.0 /usr/lib/libreoffice ; fi

########## Installing DOCCONVERTOR Service
RUN mkdir -p /opt/fcdocconverter
WORKDIR /opt/fcdocconverter
COPY fcdocconverter/FCDocConverter.jar ./
WORKDIR /etc/init.d
COPY fcdocconverter/fcdocconverter ./
RUN chmod u+x /etc/init.d/fcdocconverter
RUN update-rc.d fcdocconverter defaults
RUN service fcdocconverter start

########## Clean up
RUN rm -rf /tmp/*
RUN apt-get autoremove -y
RUN apt-get autoclean -y
RUN apt-get clean

COPY admin/script.sh /tmp/

# This results in a single layer image

EXPOSE 80